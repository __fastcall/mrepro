#ifndef __HELPER_H__
#define __HELPER_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <err.h>

/* Error checking macros. */
#define MUST_SUCCEED(e,s,x,m,...) { if ((e) != (s)) errx((x), (m), ##__VA_ARGS__); }
#define MUST_NOT_FAIL(e,f,x,m,...) { if ((e) == (f)) errx((x), (m), ##__VA_ARGS__); }

/* Warning checking macros. */
#define SHOULD_SUCCEED(e,s,m,...) { if ((e) != (s)) warnx((m), ##__VA_ARGS__); }
#define SHOULD_NOT_FAIL(e,f,m,...) { if ((e) == (f)) warnx((m), ##__VA_ARGS__); }

#endif
