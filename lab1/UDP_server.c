#include "UDP_server.h"

int main(int argc, char** argv) {

	/* Arguments with default values. */
	const char* port_arg = "1234";
	const char* payload_arg = "";

	/* Parse and decode program arguments. */
	int opt;
	while ((opt = getopt(argc, argv, "l:p:")) != -1) {
		switch (opt) {
		case 'l':
			port_arg = optarg;
			break;
		case 'p':
			payload_arg = optarg;
			break;
		default:
			fprintf(stderr, "%s\n", ERR_USAGE);
			return 1;
		}
	}
	
	/* Verify that all arguments were parsed. */
	if (optind < argc)
		errx(1, "%s\n%s", ERR_ARGCNT, ERR_USAGE);
	
	/* Address info hints. */
	struct addrinfo hints = { 0 };
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;
	
	/* Get address info results. */
	struct addrinfo* res;
	int gai = getaddrinfo(NULL, port_arg, &hints, &res);
	MUST_SUCCEED(gai, 0, 1, "%s -- %s", ERR_PORT, gai_strerror(gai));
	
	/* Create socket. */
	int sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	MUST_NOT_FAIL(sock, -1, errno, "%s (%s)", ERR_SOCKET, strerror(errno));
	
	/* Bind socket. */
	MUST_NOT_FAIL(bind(sock, res->ai_addr, res->ai_addrlen), -1, errno, "%s (%s)", ERR_BIND, strerror(errno));
	
	/* Free address info results */
	freeaddrinfo(res);
	
	/* Prepare payload response. */
	size_t resp_len = 8 + strlen(payload_arg) + 1;
	char resp[resp_len + 1];
	sprintf(resp, "PAYLOAD:%s\n", payload_arg);
	
	/* Sender and received message info. */
	ssize_t recv_len;
	char recv[RECV_MAXLEN + 1];
	struct sockaddr_in sin;
	socklen_t sin_len;
	
	/* Endless payload dispatching. */
	for (sin_len = sizeof(sin); (recv_len = recvfrom(sock, recv, RECV_MAXLEN, 0, (struct sockaddr*)&sin, &sin_len)) != -1; sin_len = sizeof(sin)) {
	
		/* Append a null terminator to the received message. */
		recv[recv_len] = '\0';
		
#ifdef DEBUG

		/* Present sender IP address and port. */
		char sender_addr[INET_ADDRSTRLEN];
		MUST_NOT_FAIL(inet_ntop(AF_INET, &(sin.sin_addr), sender_addr, INET_ADDRSTRLEN), NULL, errno, "%s (%s)", ERR_SENDERIP, strerror(errno));
		uint16_t sender_port = ntohs(sin.sin_port);
		
		/* Print received message. */
		printf("Received %d byte(s) from %s:%d\n", recv_len, sender_addr, sender_port);

#endif

		/* Check if message is HELLO. */
		if (strcmp(recv, "HELLO") == 0) {
			
			/* Send response with payload. */
			ssize_t sent_len = sendto(sock, resp, resp_len, 0, (struct sockaddr*)&sin, sin_len);
			MUST_NOT_FAIL(sent_len, -1, errno, "%s (%s)", ERR_SENDFAIL, strerror(errno));
			
#ifdef DEBUG

			/* Print sent message. */
			printf("Sent %d byte(s) to %s:%d\n", sent_len, sender_addr, sender_port);

#endif
			
		}
	
	}
	
	/* Failed to receive data (normally this code isn't reached unless an error occurs). */
	errx(errno, "%s (%s)", ERR_RECVFAIL, strerror(errno));
	
	return 0;

}
