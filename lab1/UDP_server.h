#ifndef __UDP_SERVER_H__
#define __UDP_SERVER_H__

#include "helper.h"

#define ERR_USAGE "Usage: ./UDP_server [-l port] [-p payload]"
#define ERR_ARGCNT "invalid argument count"
#define ERR_PORT "invalid port"
#define ERR_SOCKET "socket creation failed"
#define ERR_BIND "socket binding failed"
#define ERR_SENDERIP "invalid sender IP address"
#define ERR_SENDFAIL "failed to send data"
#define ERR_RECVFAIL "failed to receive data"

#define RECV_MAXLEN 256

#endif
