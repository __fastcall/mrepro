/* Shared header. */
#include "shared.h"

/* Program arguments. */
static const char* g_TcpService = "1234";
static const char* g_UdpService = "1234";
static char g_Payloads[PAYLOADS_LEN + 1] = { ':', '\n' };
static unsigned short g_PayloadsLength = 2;

/* Function forward-declarations. */
static BOOL arg_callback(int, const char*);
static BOOL set_payloads(const char*);

/* Entry function. */
int main(int argc, char** argv) {
	
	/* Parse command-line arguments. */
	if (Parseargs(argc, argv, "t:u:p:", arg_callback, 0) == FALSE) {
		fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n", argv[0]);
		return 1;
	}
	
	/* Get server addresses. */
	struct sockaddr_in addr_tcp, addr_udp;
	GETADDRINFO(NULL, g_TcpService, PROTOCOL_TCP, &addr_tcp);
	GETADDRINFO(NULL, g_UdpService, PROTOCOL_UDP, &addr_udp);
	
	/* Create and bind sockets. */
	int sock_tcp, sock_udp;
	CREATEBINDSOCKET(TYPE_STREAM, PROTOCOL_TCP, &addr_tcp, &sock_tcp);
	LISTEN(sock_tcp, 1);
	CREATEBINDSOCKET(TYPE_DATAGRAM, PROTOCOL_UDP, &addr_udp, &sock_udp);
	
	/* Set sockets reusable address. */
	SETSOCKOPT_REUSEADDR(sock_tcp, TRUE);
	SETSOCKOPT_REUSEADDR(sock_udp, TRUE);
	
	/* Endless command loop. */
	while (TRUE) {
		
		/* File descriptors. */
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(STDIN, &fds); FD_SET(sock_tcp, &fds); FD_SET(sock_udp, &fds);
		
		/* Select file descriptors. */
		SELECT(sock_udp + 1, &fds, NULL, NULL);
		
		/* Check if standard input is ready. */
		if (FD_ISSET(STDIN, &fds)) {
			
			/* Read from standard input. */
			char input[MAX_LINE];
			fgets(input, MAX_LINE, stdin);
			
			/* Decode and execute command. */
			if (strcmp(input, CMD_PRINT) == 0) {
				printf("%s", g_Payloads);
			} else if (strcmp(input, CMD_QUIT) == 0) {
				Close(sock_tcp);
				Close(sock_udp);
				return 0;
			} else {
				char* arguments = strchr(input, ' ');
				if (arguments != NULL) {
					*arguments++ = '\0';
					if (strcmp(input, CMD_SET) == 0) {
						if (set_payloads(arguments) == FALSE) warnx("Invalid payloads format.");
					} else {
						warnx("Unknown command.");
					}
				} else {
					warnx("Unknown command.");
				}
			}
			
		}
		
		/* Check if TCP socket is ready. */
		if (FD_ISSET(sock_tcp, &fds)) {
			
			/* Accept TCP client. */
			int sock_client;
			struct sockaddr_in addr_client;
			if (Accept(sock_tcp, &addr_client, &sock_client) == TRUE) {
				
				/* Receive command and respond with payloads if it is hello command. */
				char buffer[strlen(CMD_HELLO)];
				if ((Recvn(sock_client, buffer, sizeof(buffer)) == TRUE) && (memcmp(buffer, CMD_HELLO, sizeof(buffer)) == 0)) {
					Sendn(sock_client, g_Payloads, g_PayloadsLength);
				}
				
				/* Close client socket. */
				Close(sock_client);
				
			}
			
		}
		
		/* Check if UDP socket is ready. */
		if (FD_ISSET(sock_udp, &fds)) {
			
			/* Receive command and respond with payloads if it is hello command. */
			struct sockaddr_in addr;
			char buffer[strlen(CMD_HELLO)] = { 0 };
			if ((Recvfrom(sock_udp, buffer, sizeof(buffer), &addr, NULL) == TRUE) && (memcmp(buffer, CMD_HELLO, sizeof(buffer)) == 0)) {
				Sendto(sock_udp, g_Payloads, g_PayloadsLength, &addr, NULL);
			}
			
		}
		
	}
	
	/* Exit success. */
	return 0;
	
}

/* Parse arguments callback. */
BOOL arg_callback(int flag, const char* arg) {
	switch (flag) {
	case 't': g_TcpService = arg; return TRUE;
	case 'u': g_UdpService = arg; return TRUE;
	case 'p': return set_payloads(arg);
	}
	return FALSE;
}

/* Set payload argument. */
BOOL set_payloads(const char* payload) {
	
	/* Find line-feed position and length until its first occurrence. */
	const char* lfpos = strchr(payload, '\n');
	size_t len = (lfpos == NULL) ? strlen(payload) : (lfpos - payload);
	
	/* If string ends with line-feed or ':' character, ignore it. */
	if ((len > 0) && (payload[len - 1] == '\n')) len--;
	if ((len > 0) && (payload[len - 1] == ':')) len--;
	
	/* Assert that string is not too long. */
	if (len > PAYLOADS_LEN - 2) return FALSE;
	
	/* Copy payload then append ':' and line-feed. */
	memcpy(g_Payloads, payload, len);
	g_Payloads[len] = ':';
	g_Payloads[len + 1] = '\n';
	g_Payloads[len + 2] = '\0';
	
	/* Write length parameter and return success. */
	g_PayloadsLength = len + 2;
	return TRUE;
	
}
