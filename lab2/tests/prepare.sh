#!/usr/local/bin/bash

# Copy lab files
hcp ../bot bot1:/bot
hcp ../bot bot2:/bot
hcp ../bot bot3:/bot
hcp ../bot bot4:/bot
hcp ../server server1:/server
hcp ../server server2:/server
hcp ../CandC.py cc:/CandC.py

# Copy test files
hcp cc1.py cc:/cc1.py
hcp cc2.py cc:/cc2.py
