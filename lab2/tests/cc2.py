import socket
import sys
import select
import subprocess
import time

def main():
	clients = []
	udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	udp_sock.bind(('', 3333))
	for _ in range(4):
		data, addr = udp_sock.recvfrom(4)
		assert data == 'REG\n', 'Invalid registration message.'
		clients.append(addr)
		print addr
		pass
	time.sleep(5)
	for addr in clients:
		udp_sock.sendto('210.0.8.20\0      6969\0                 ', addr)
		pass
	time.sleep(5)
	for addr in clients:
		udp_sock.sendto('310.0.6.20\0      1337\0                 10.0.4.255\0     1337\0                 10.0.5.21\0      1337\0                 ', addr)
		pass
	pass

if __name__ == '__main__':
	main()
