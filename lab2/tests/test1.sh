#!/usr/local/bin/bash

# Run server
xfce4-terminal --command 'himage server1 /server -t 6969 -p payload1:payload2:payload3' --title 'server' &

# Run C&C
xfce4-terminal --command 'himage cc python2 cc1.py' --title 'C&C' &
sleep 5

# Run bots
xfce4-terminal --command 'himage bot1 /bot 10.0.7.21 3333' --title 'bot1' &
xfce4-terminal --command 'himage bot2 /bot 10.0.7.21 3333' --title 'bot2' &
xfce4-terminal --command 'himage bot3 /bot 10.0.7.21 3333' --title 'bot3' &
xfce4-terminal --command 'himage bot4 /bot 10.0.7.21 3333' --title 'bot4' &
