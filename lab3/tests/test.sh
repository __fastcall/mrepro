#!/bin/sh

# Exit if IMUNES simulation is not running
imunes_not_running() {
	echo "Is IMUNES simulation running? Try Experiment -> Execute."
	exit 1
}

# Assure that IMUNES simulation is running
himage -e CnC > /dev/null 2>&1 || imunes_not_running

# Build lab 3
cd ..
make
cd tests

# Copy C&C
himage CnC mkdir /CandC
hcp runcc.sh CnC:/CandC/
hcp ../CandC CnC:/CandC/
hcp ../htdocs/* CnC:/CandC/

# Copy bot
hcp ../bot BOT1:/
hcp ../bot BOT3:/

# Copy server
hcp ../server Server:/

# Run programs
xfce4-terminal -T 'CandC' -e "bash -c 'himage CnC /bin/sh /CandC/runcc.sh';bash"
xfce4-terminal -T 'Server' -e "bash -c 'himage Server /server -p \"First payload:Second payload:Third payload\"';bash"
xfce4-terminal -T 'zrtva1' -e "bash -c 'himage zrtva1 nc -kul 1111';bash"
xfce4-terminal -T 'zrtva2' -e "bash -c 'himage zrtva2 nc -kul 2222';bash"
xfce4-terminal -T 'zrtva3' -e "bash -c 'himage zrtva3 nc -kul 3333';bash"
xfce4-terminal -T 'bot1' -e "bash -c 'himage BOT1 /bot 10.0.0.10 5555';bash"
xfce4-terminal -T 'bot3' -e "bash -c 'himage BOT3 /bot 10.0.0.10 5555';bash"
