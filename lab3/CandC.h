#ifndef __CANDC_H__
#define __CANDC_H__

/* Shared header. */
#include "shared.h"

/* Data header. */
#include "data.h"

/* HTTP service port (default). */
#define HTTP_SERVICE_PORT "80" // "http"

/* C&C service port. */
#define CC_SERVICE_PORT "5555"

/* Maximum HTTP connections at a time. */
#define MAX_HTTP_CONNECTIONS 32

/* Maximum connected bots. */
#define MAX_BOT_CONNECTIONS 32

#endif
