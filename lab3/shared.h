#ifndef __SHARED_H__
#define __SHARED_H__

/* High-level wrappers. */
#include <wrappers.h>

/* Optional: define if you want to include the line-feed character at the end of each payload. */
#define PAYLOAD_APPEND_LF

/* Maximum time (in seconds) to wait for payloads from TCP/UDP server. */
#define PAYLOAD_TIMEOUT 5

/* Global constants. */
#define HOSTNAME_LEN INET_ADDRSTRLEN
#define SERVICE_LEN 22
#define ENDPOINT_COUNT 20
#define PAYLOADS_LEN 1024
#define REPEAT 100
#define MAX_LINE 2048

/* Known command strings. */
#define CMD_REG "REG\n"
#define CMD_HELLO "HELLO\n"
#define CMD_PRINT "PRINT\n"
#define CMD_QUIT "QUIT\n"
#define CMD_SET "SET" // no newline, parameter expected

/* Counts the total amount of endpoints specified within the message structure. */
#define COUNT_ENDPOINTS(n) (((n) - sizeof(COMMAND)) / sizeof(ENDPOINT))

/* Known commands. */
enum COMMAND {
	Quit = '0',
	Prog_TCP = '1',
	Prog_UDP = '2',
	Run = '3',
	Stop = '4'
};

/* Message command. */
typedef char COMMAND;

/* Message endpoint structure. */
typedef struct __attribute__((packed)) {
	char hostname[HOSTNAME_LEN];
	char service[SERVICE_LEN];
} ENDPOINT;

/* Message structure. */
typedef struct __attribute__((packed)) {
	COMMAND command;
	ENDPOINT endpoints[ENDPOINT_COUNT];
} MSG;

#endif
