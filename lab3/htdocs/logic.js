function do_action(path) {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			if (this.status == 200) {
				window.alert("Command issued.");
			} else {
				window.alert("Unknown error.");
			}
		}
	};
	xhttp.open("GET", path, true);
	xhttp.send();
}

function do_action_nomsg(path) {
	let xhttp = new XMLHttpRequest();
	xhttp.open("GET", path, true);
	xhttp.send();
}

function quit_srv() {
	do_action_nomsg("/bot/quit");
	document.body.innerHTML = "Bye.";
}

function refresh_bots() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			if (this.status == 200) {
				let response = JSON.parse(this.responseText);
				let bots = response["bots"];
				let str = "<div>";
				for (let i = 0; i < bots.length; ++i) {
					str += "<p class=\"par\">" + bots[i][0] + ":" + bots[i][1] + "</p>";
				}
				if (bots.length == 0) {
					str += "<p class=\"par\">No connected bots</p>";
				}
				str += "</div>";
				document.getElementById("bots").innerHTML = str;
			} else {
				document.getElementById("bots").innerHTML = "<p class=\"par\">Unknown error.</p>";
			}
		}
	};
	xhttp.open("GET", "/bot/listjson", true);
	xhttp.send();
}
