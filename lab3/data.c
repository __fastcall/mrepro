/* Data header. */
#include "data.h"

/* Structure for 'pt' command. */
const MSG pt_data = { Prog_TCP, { { "10.0.0.20", "1234" } } };
const size_t pt_size = sizeof(COMMAND) + 1 * sizeof(ENDPOINT);

/* Structure for 'ptl' command. */
const MSG ptl_data = { Prog_TCP, { { "127.0.0.1", "1234" } } };
const size_t ptl_size = sizeof(COMMAND) + 1 * sizeof(ENDPOINT);

/* Structure for 'pu' command. */
const MSG pu_data = { Prog_UDP, { { "10.0.0.20", "1234" } } };
const size_t pu_size = sizeof(COMMAND) + 1 * sizeof(ENDPOINT);

/* Structure for 'pul' command. */
const MSG pul_data = { Prog_UDP, { { "127.0.0.1", "1234" } } };
const size_t pul_size = sizeof(COMMAND) + 1 * sizeof(ENDPOINT);

/* Structure for 'r' command. */
const MSG r_data = { Run, { { "127.0.0.1", "vat" }, { "localhost", "6789" } } };
const size_t r_size = sizeof(COMMAND) + 2 * sizeof(ENDPOINT);

/* Structure for 'r2' command. */
const MSG r2_data = { Run, { { "20.0.0.11", "1111" }, { "20.0.0.12", "2222" }, { "20.0.0.13", "dec-notes" } } };
const size_t r2_size = sizeof(COMMAND) + 3 * sizeof(ENDPOINT);

/* Structure for 's' command. */
const MSG s_data = { Stop };
const size_t s_size = sizeof(COMMAND) + 0 * sizeof(ENDPOINT);

/* Structure for 'n' command. */
const char* n_data = "NEPOZNATA\n";
const size_t n_size = 10;

/* Structure for 'q' command. */
const MSG q_data = { Quit };
const size_t q_size = sizeof(COMMAND) + 0 * sizeof(ENDPOINT);
