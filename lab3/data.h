#ifndef __DATA_H__
#define __DATA_H__

/* Shared header. */
#include "shared.h"

/* Structure for 'pt' command. */
extern const MSG pt_data;
extern const size_t pt_size;

/* Structure for 'ptl' command. */
extern const MSG ptl_data;
extern const size_t ptl_size;

/* Structure for 'pu' command. */
extern const MSG pu_data;
extern const size_t pu_size;

/* Structure for 'pul' command. */
extern const MSG pul_data;
extern const size_t pul_size;

/* Structure for 'r' command. */
extern const MSG r_data;
extern const size_t r_size;

/* Structure for 'r2' command. */
extern const MSG r2_data;
extern const size_t r2_size;

/* Structure for 's' command. */
extern const MSG s_data;
extern const size_t s_size;

/* Structure for 'n' command. */
extern const char* n_data;
extern const size_t n_size;

/* Structure for 'q' command. */
extern const MSG q_data;
extern const size_t q_size;

#endif
