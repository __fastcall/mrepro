#!/usr/local/bin/bash

# Build lab 1
make -C ../../lab1

# Copy programs to machines
hcp CandC.py pc1:/home/imunes/CandC.py
hcp ../../lab1/UDP_server pc2:/home/imunes/UDP_server
hcp ../../lab1/bot pc3:/home/imunes/bot
hcp ../../lab1/bot pc4:/home/imunes/bot
