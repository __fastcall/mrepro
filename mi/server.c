#include "server.h"

/* Arguments. */
char* g_TcpPort = "1234";
char* g_Timeout = "20";
IOTPairs g_Pairs[IOT_COUNT] = { {0} };
int g_PairCount = 0;

struct sockaddr_in g_ServerAddrInfo = { 0 };
long g_TimeoutSeconds = 20;


int main(int argc, char** argv) {
	
	/* Parse command-line arguments. */
	int opt;
	while ((opt = getopt(argc, argv, "p:t:")) != -1) {
		switch (opt) {
		case 'p':
			g_TcpPort = optarg;
			break;
		case 't':
			g_Timeout = optarg;
			break;
		default:
			errx(1, VALID_USAGE);
			break;
		}
	}
	
	/* Get address info for this server. */
	if (Getaddrinfo(NULL, g_TcpPort, PROTOCOL_TCP, &g_ServerAddrInfo) == FALSE)
		errx(1, "Invalid service port -- %s\n%s", g_TcpPort, VALID_USAGE);
		
	/* Try parse timeout seconds. */
	g_TimeoutSeconds = strtol(g_Timeout, NULL, 10);
	if ((g_TimeoutSeconds == 0) && ((errno == EINVAL) || (errno == ERANGE)))
		errx(1, "Invalid timeout value -- %s\n%s", g_Timeout, VALID_USAGE);
		
	/* At least one IOT device must be specified. */
	if (argc - optind < 2)
		errx(1, "At least one device must be specified.\n%s", VALID_USAGE);
		
	/* Argument count must be even. */
	if ((argc - optind) % 2 == 1)
		errx(1, "Uneven argument count.\n%s", VALID_USAGE);
		
	/* Calculate the number of pairs. */
	g_PairCount = (argc - optind) / 2;
	
	/* Parse all pairs. */	
	for (int i = 0; i < g_PairCount; ++i) {
		g_Pairs[i].ipaddr = argv[optind + 2 * i];
		g_Pairs[i].port = argv[optind + 2 * i + 1];
		if (Getaddrinfo(g_Pairs[i].ipaddr, g_Pairs[i].port, PROTOCOL_UDP, &(g_Pairs[i].sin)) == FALSE)
			errx(1, "Invalid IOT device - %s:%s", g_Pairs[i].ipaddr, g_Pairs[i].port);
	}
	
	/* Create sockets for IOT devices and notify them, also init SO_RCVTIMEO value to provided seconds. */
	for (int i = 0; i < g_PairCount; ++i) {
		if (Socket(TYPE_DATAGRAM, PROTOCOL_UDP, &(g_Pairs[i].sock)) == FALSE)
			errx(1, "Socket could not be created (IOT device).");
		if (Sendto(g_Pairs[i].sock, MSG_INIT, strlen(MSG_INIT), &(g_Pairs[i].sin), NULL) == FALSE)
			errx(1, "Could not send initialization message to IOT device socket.");
		if (SetSockOpt_RcvTimeo(g_Pairs[i].sock, g_TimeoutSeconds, 0) == FALSE)
			errx(1, "Could not set recieve timeout value of IOT device socket.");
	}
	
	/* Create TCP server socket and assign SO_REUSEADDR attribute. */
	int sock;
	if ((CreateBindSocket(TYPE_STREAM, PROTOCOL_TCP, &g_ServerAddrInfo, &sock) == FALSE) || (SetSockOpt_ReuseAddr(sock, TRUE) == FALSE))
		errx(1, "Could not create a TCP server socket");
		
	/* Begin listening. */
	if (Listen(sock, 1) == FALSE)
		errx(1, "Could not begin a TCP listener.");
	
	/* Endless serve. */
	while (TRUE) {
		
		/* Accept client. */
		struct sockaddr_in cliaddr;
		int clisock;
		if (Accept(sock, &cliaddr, &clisock) == FALSE) {
			fprintf_cond(stderr, "Client TCP connection rejected\n");
			continue;
		}
		
		/* Print connect message. */
		char ip[NTOP_MAXLEN];
		unsigned short port = ntohs(cliaddr.sin_port);
		if (Ntop(&cliaddr.sin_addr, ip, sizeof(ip)) == FALSE) {
			fprintf_cond(stderr, "Client TCP connection rejected\n");
			close(clisock);
			continue;
		};
		fprintf(stderr, "Spojio se: %s:%d\n", ip, port);
		
		/* Endless receive commands and answer. */
		while (TRUE) {
			
			/* Receive command from TCP master. */
			char rcvbuff[COMMAND_MAXLEN];
			size_t rcvcnt;
			BOOL isEOF;
			if (Recvuntil(clisock, rcvbuff, sizeof(rcvbuff), '\n', &rcvcnt, &isEOF) == FALSE) {
				fprintf_cond(stderr, "Client TCP receive failed\n");
				close(clisock);
				break;
			}
			
			/* Empty buffer, eof */
			if (isEOF) {
				fprintf(stderr, "Odspojio se: %s:%d\n", ip, port);
				break;
			}
			
			/* Replace line feed char with null terminator. */
			rcvbuff[rcvcnt - 1] = '\0';
			
			/* Command multiplexing. */
			fprintf(stderr, "Naredba: %s\n", rcvbuff);
			if (strcmp(rcvbuff, "status") == 0) {
				
				/* Send number of IoT devices. */
				char resp[3] = { g_PairCount + '0', '\n', '\0' };
				if (Sendn(clisock, resp, sizeof(resp)) == FALSE) {
					fprintf_cond(stderr, "Response to status command failed.\n");
					close(clisock);
					break;
				}
				
				/* Print what you did. */
				printf_cond("Responded to status command with %i\n", g_PairCount);
				
			} else if ((rcvcnt == 7) && (ISDATACMD(rcvbuff))) {
				
				/* Decode which 0-indexed sensor is required. */
				int num = GETDATANUM(rcvbuff) - 1;
				
				/* Make sure that index is within bounds. */
				if ((num >= 0) && (num < g_PairCount)) {
					
					/* Sensor shorthand. */
					IOTPairs* sensor = &(g_Pairs[num]);
					
					/* Send stat message. */
					if (Sendto(sensor->sock, MSG_STAT, strlen(MSG_STAT), &(sensor->sin), NULL) == FALSE) {
						
						/* Send error message. */
						if (Sendn(clisock, MSG_ERROR, strlen(MSG_ERROR)) == FALSE) {
							fprintf_cond(stderr, "Response to data command failed.\n");
							close(clisock);
							break;
						}
						
						/* Sensor unqueryable. */
						fprintf_cond(stderr, "Response to data command failed (sensor %i unqueryable).\n", num + 1);
						continue;
						
					}
					
					/* Sensor response. */
					char data[SENSOR_MAXLEN];
					size_t datalen;
					
					/* Receive data from sensor. */
					if (Recvfrom(sensor->sock, data, sizeof(data), &(sensor->sin), &datalen) == TRUE) {
						
						/* Propagate data. */
						if (Sendn(clisock, data, datalen) == FALSE) {
							fprintf_cond(stderr, "Response to data command failed.\n");
							close(clisock);
							break;
						}
						
						/* Print tip. */
						fprintf_cond(stderr, "Response to data command from sensor %i.\n", num + 1);
						
					} else {
					
						/* Send error message. */
						if (Sendn(clisock, MSG_ERROR, strlen(MSG_ERROR)) == FALSE) {
							fprintf_cond(stderr, "Response to data command failed.\n");
							close(clisock);
							break;
						}
						
						/* Timeout warning. */
						fprintf_cond(stderr, "Response to data command failed (sensor %i timeout).\n", num + 1);
					
					}
					
				} else {
					
					/* Send error message. */
					if (Sendn(clisock, MSG_ERROR, strlen(MSG_ERROR)) == FALSE) {
						fprintf_cond(stderr, "Response to data command failed.\n");
						close(clisock);
						break;
					}
					
					/* Out of bounds warning. */
					fprintf_cond(stderr, "Response to malformed data command (index %i unregistered).\n", num + 1);
					
				}
				
			} else if (strcmp(rcvbuff, "avg") == 0) {
				
				/* Calculate average. */
				long avg;
				if (fetchavg(&avg) == TRUE) {
					
					/* Prepare message for sending. */
					char avgstr[SENSOR_MAXLEN + 2]; // 1 zbog newlinea, 1 zbog null terminatora (iz senzora ne moraju nuzno imati to)
					sprintf(avgstr, "%li\n", avg);
					
					/* Send error message. */
					if (Sendn(clisock, avgstr, strlen(avgstr)) == FALSE) {
						fprintf_cond(stderr, "Response to avg command failed.\n");
						close(clisock);
						break;
					}
					
					/* Out of bounds warning. */
					fprintf_cond(stderr, "Responded to avg command from all devices.\n");
					
				} else {
					
					/* Send error message. */
					if (Sendn(clisock, MSG_ERROR, strlen(MSG_ERROR)) == FALSE) {
						fprintf_cond(stderr, "Response to avg command failed.\n");
						close(clisock);
						break;
					}
					
					/* Out of bounds warning. */
					fprintf_cond(stderr, "Response to avg command failed (no responses from all IOT devices).\n");
					
				}
				
			} else if (strcmp(rcvbuff, "quit") == 0) {
			
				/* Quit msg print. */
				fprintf(stderr, "Odspojio se: %s:%d\n", ip, port);
				close(clisock);
				break;
			
			} else {
				
				/* Unknown command. */
				fprintf_cond(stderr, "Unknown command: %s\n", rcvbuff);
				
			}
			
		}
		
	}
	
	/* Never reach. */
	return 0;
}

BOOL fetchavg(long* out) {
	
	long sum = 0;
	
	for (int i = 0; i < g_PairCount; ++i) {
		
		/* Sensor shorthand. */
		IOTPairs* sensor = &(g_Pairs[i]);
					
		/* Send stat message. */
		if (Sendto(sensor->sock, MSG_STAT, strlen(MSG_STAT), &(sensor->sin), NULL) == FALSE) {
			return FALSE;
		}
		
		/* Sensor response. */
		char data[SENSOR_MAXLEN + 1];
		size_t datalen;
					
		/* Receive data from sensor. */
		if (Recvfrom(sensor->sock, data, sizeof(data) - 1, &(sensor->sin), &datalen) == FALSE) {
			return FALSE;
		}
		data[SENSOR_MAXLEN] = '\0';
		
		/* Read temporary sensor value. */
		long tmp = strtol(data, NULL, 10);
		if ((tmp == 0) && ((errno == EINVAL) || (errno == ERANGE))) {
			return FALSE;
		}
		
		/* Add to sum. */
		sum += tmp;
		
	}
	
	/* Write out. */
	*out = sum / g_PairCount;
	return TRUE;
	
}
