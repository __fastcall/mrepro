#ifndef __SERVER__
#define __SERVER__

#include "wrappers.h"
#include <stdio.h>
#include <stdlib.h>

/* za printanje mojih pomocnih poruka */
//#define PRINT_MYMSGS

#define IOT_COUNT 5
#define COMMAND_MAXLEN 64
#define SENSOR_MAXLEN 512

#define VALID_USAGE "Usage: server [-p tcp_port] [-t timeout] ip1 udp1 {ip2 udp2 .. ip5 udp5}"

#define MSG_INIT "INIT\n"
#define MSG_STAT "STAT\n"
#define MSG_ERROR "ERROR\n"

/* Prljavo !!! */
#define ISDATACMD(x) ((x)[0] == 'd' && (x)[1] == 'a' && (x)[2] == 't' && (x)[3] == 'a' && (x)[4] == ' ')
#define GETDATANUM(x) ((x)[5] - '0')

#ifdef PRINT_MYMSGS
#define printf_cond(...) {printf(__VA_ARGS__);}
#define fprintf_cond(...) {fprintf(__VA_ARGS__);}
#else
#define printf_cond(...) {}
#define fprintf_cond(...) {}
#endif

/* IOT devide IP:Port pair. */
typedef struct {
	char* ipaddr;
	char* port;
	struct sockaddr_in sin;
	int sock;
} IOTPairs;

extern BOOL fetchavg(long* out);

#endif
