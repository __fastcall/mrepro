#ifndef __SHARED_H__
#define __SHARED_H__

/* GLOBAL OPTIONS. */
#define OPT_ERRCODE_HWORD /* Define to interpret ERRCODE in hardware byte order. Otherwise it will be interpreted in network byte order. Every other numeric value will still remain interpreted in network byte order regardless of this definition. */
// #define OPT_ENFORCE_RFC1350 /* Define to enforce RFC 1350 standard only. */

/* My wrappers library. */
#include <wrappers.h>

/* Operation codes. */
enum OPCODE {
	ReadRequest = 1,      /* RRQ */
	WriteRequest = 2,     /* WRQ */
	Data = 3,             /* DATA */
	Acknowledgement = 4,  /* ACK */
	Error = 5             /* ERROR */
};
typedef uint16_t OPCODE;

/* Error codes. */
enum ERRCODE {
	NotDefined = 0,
	FileNotFound = 1,
	AccessViolation = 2,
	DiskFull = 3,
	IllegalTFTPOperation = 4,
	UnknownTransferID = 5,
	FileAlreadyExists = 6,
	NoSuchUser = 7
};
typedef uint16_t ERRCODE;

/* Request packet structure. */
typedef struct __attribute__((packed)) {
	OPCODE opcode;        /* RRQ or WRQ */
	char arguments[0];    /* Filename + Transfer mode */
} REQUEST;

/* Data packet structure. */
typedef struct __attribute__((packed)) {
	OPCODE opcode;        /* DATA */
	uint16_t blkid;       /* Block ID */
	char data[0];         /* File data */
} DATA;

/* Acknowledgement packet structure. */
typedef struct __attribute__((packed)) {
	OPCODE opcode;        /* ACK */
	uint16_t blkid;       /* Block ID */
} ACK;

/* Error packet structure. */
typedef struct __attribute__((packed)) {
	OPCODE opcode;        /* ERROR */
	ERRCODE errcode;      /* Error code */
	char errmsg[0];       /* Error message */
} ERROR;

#endif
