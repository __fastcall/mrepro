#ifndef __ERRORSTR_H__
#define __ERRORSTR_H__

/* Error message strings. */
#define ERRSTR_INTERNAL "An internal error has occurred"
#define ERRSTR_PATH "Requested file path is not permitted"
#define ERRSTR_EXISTS "Requested file already exists"
#define ERRSTR_DISKFULL "Not enough disk space remaining"
#define ERRSTR_CREATION "Requested file could not be created"
#define ERRSTR_NOTFOUND "Requested file does not exist"
#define ERRSTR_OPEN "Requested file could not be opened"

#endif
