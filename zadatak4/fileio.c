/* File IO header. */
#include "fileio.h"

/* NetASCII IO operations header. */
#include "netascii.h"

/* Error message strings header. */
#include "errorstr.h"

/* Configuration of error code byte order. */
#ifdef OPT_ERRCODE_HWORD
#define ERRCODE_NTOHS(x) (x)
#define ERRCODE_HTONS(x) (x)
#else
#define ERRCODE_NTOHS(x) (ntohs(x))
#define ERRCODE_HTONS(x) (htons(x))
#endif

/* Converts transfer mode number to transfer mode string (internal). */
static BOOL transmode_num_to_str(TRANSMODE mode, const char** strmode) {
	switch (mode) {
	case NetASCII: *strmode = STR_NETASCII; return TRUE;
	case Octet: *strmode = STR_OCTET; return TRUE;
	}
	return FALSE;
}

/* Converts transfer mode string to transfer mode number (internal). */
static BOOL transmode_str_to_num(const char* strmode, TRANSMODE* mode) {
	BOOL equals;
	if (CompareStrings_IgnoreCase(strmode, STR_NETASCII, &equals) == FALSE) return FALSE;
	if (equals == TRUE) { *mode = NetASCII; return TRUE; }
	if (CompareStrings_IgnoreCase(strmode, STR_OCTET, &equals) == FALSE) return FALSE;
	if (equals == TRUE) { *mode = Octet; return TRUE; }
	return FALSE;
}

/* Unpack request packet (internal). */
static BOOL unpack_rq_packet(OPCODE opcode, const REQUEST* packet, size_t packetsize, char* filename, size_t nfilename, TRANSMODE* mode) {
	
	/* Assure that packet has its minimum size. */
	if (packetsize < sizeof(REQUEST) + 2) return FALSE;
	
	/* Assure that packet has the desired opcode set. */
	if (packet->opcode != htons(opcode)) return FALSE;
	
	/* Assure that packet ends with null-termination character. */
	const char* data = (const char*)packet;
	if (data[packetsize - 1] != '\0') return FALSE;
	
	/* Fetch filename argument and its length. */
	const char* arg_filename = data + sizeof(REQUEST);
	size_t arg_filename_len = strlen(arg_filename);
	
	/* Assure that packet contains at least one more argument. */
	if (arg_filename_len == packetsize - sizeof(REQUEST) - 1) return FALSE;
	
	/* Fetch transfer mode argument. */
	const char* arg_transmode = arg_filename + arg_filename_len + 1;
	
#ifdef OPT_ENFORCE_RFC1350

	/* Count transfer mode string length. */
	size_t arg_transmode_len = strlen(arg_transmode);

	/* Assure that packet contains no more arguments. */
	if (arg_transmode_len != packetsize - sizeof(REQUEST) - arg_filename_len - 2) return FALSE;
	
#endif
	
	/* Assure that buffer is long enough to contain filename argument. */
	if (nfilename < arg_filename_len + 1) return FALSE;
	
	/* Decode transfer mode. */
	if (transmode_str_to_num(arg_transmode, mode) == FALSE) return FALSE;
	
	/* Copy filename. */
	memcpy(filename, arg_filename, arg_filename_len + 1);
	
	/* Return success. */
	return TRUE;
	
}

/* Pack request packet (internal). */
static BOOL pack_rq_packet(OPCODE opcode, REQUEST* packet, size_t packetsize, const char* filename, TRANSMODE mode, size_t* totalbytes) {
	
	/* Calculate filename length. */
	size_t filename_len = strlen(filename);
	
	/* Get transfer mode string. */
	const char* strmode;
	if (transmode_num_to_str(mode, &strmode) == FALSE) return FALSE;
	
	/* Calculate transfer mode string length. */
	size_t strmode_len = strlen(strmode);
	
	/* Assure that packet is long enough to contain required data. */
	size_t reqbytes = sizeof(REQUEST) + filename_len + strmode_len + 2;
	if (packetsize < reqbytes) return FALSE;
	
	/* Set desired opcode. */
	packet->opcode = htons(opcode);
	
	/* Copy filename and transfer mode string. */
	memcpy(packet->arguments, filename, filename_len + 1);
	memcpy(packet->arguments + filename_len + 1, strmode, strmode_len + 1);
	
	/* Write total bytes. */
	if (totalbytes != NULL) *totalbytes = reqbytes;
	
	/* Return success. */
	return TRUE;
	
}

/* Unpack read request packet. */
BOOL unpack_rrq_packet(const REQUEST* packet, size_t packetsize, char* filename, size_t nfilename, TRANSMODE* mode) {
	return unpack_rq_packet(ReadRequest, packet, packetsize, filename, nfilename, mode);
}

/* Pack read request packet. */
BOOL pack_rrq_packet(REQUEST* packet, size_t packetsize, const char* filename, TRANSMODE mode, size_t* totalbytes) {
	return pack_rq_packet(ReadRequest, packet, packetsize, filename, mode, totalbytes);
}

/* Unpack write request packet. */
BOOL unpack_wrq_packet(const REQUEST* packet, size_t packetsize, char* filename, size_t nfilename, TRANSMODE* mode) {
	return unpack_rq_packet(WriteRequest, packet, packetsize, filename, nfilename, mode);
}

/* Pack write request packet. */
BOOL pack_wrq_packet(REQUEST* packet, size_t packetsize, const char* filename, TRANSMODE mode, size_t* totalbytes) {
	return pack_rq_packet(WriteRequest, packet, packetsize, filename, mode, totalbytes);
}

/* Unpack data packet. */
BOOL unpack_data_packet(const DATA* packet, size_t packetsize, uint16_t* blkid, FILE* file, TRANSMODE mode, BOOL* isfinal, BOOL* breakconn) {
	
	/* Set break connection to false. */
	if (breakconn != NULL) *breakconn = FALSE;
	
	/* Assure that packet size is within specified limits. */
	if ((packetsize < sizeof(DATA)) || (packetsize > sizeof(DATA) + CHUNK_SIZE)) return FALSE;
	
	/* Assure that packet has appropriate opcode. */
	if (packet->opcode != htons(Data)) return FALSE;
	
	/* Fetch packet data and its length. */
	const char* data = packet->data;
	size_t data_len = packetsize - sizeof(DATA);
	
	/* Write to file in desired transfer mode. */
	long fpos;
	switch (mode) {
	case NetASCII:
		if (FileWriteNetASCII(data, data_len, file, NULL, breakconn) == FALSE) return FALSE;
		break;
	case Octet:
		if ((fpos = ftell(file)) == -1) return FALSE;
		if (fwrite(data, 1, data_len, file) != data_len) {
			if (fseek(file, fpos, SEEK_SET) != 0) {
				if (breakconn == NULL) err(1, ERR_UNPACK_DATA);
				*breakconn = TRUE;
			}
			return FALSE;
		}
		break;
	default:
		return FALSE;
	}
	
	/* Truncate file if final packet. */
	if (data_len < CHUNK_SIZE) {
		if (((fpos = ftell(file)) == -1) || (ftruncate(fileno(file), fpos) != 0)) {
			if (breakconn == NULL) err(1, ERR_UNPACK_DATA);
			*breakconn = TRUE;
			return FALSE;
		}
	}
	
	/* Set block ID. */
	*blkid = ntohs(packet->blkid);
	
	/* Set if packet is final. */
	if (isfinal != NULL) *isfinal = (data_len < CHUNK_SIZE) ? TRUE : FALSE;
	
	/* Return success. */
	return TRUE;
	
}

/* Pack data packet. */
BOOL pack_data_packet(DATA* packet, size_t packetsize, uint16_t blkid, FILE* file, TRANSMODE mode, size_t* totalbytes, BOOL* isfinal, BOOL* breakconn) {
	
	/* Set break connection to false. */
	if (breakconn != NULL) *breakconn = FALSE;
	
	/* Assure that packet size is within specified limits. */
	if ((packetsize < sizeof(DATA)) || (packetsize > sizeof(DATA) + CHUNK_SIZE)) return FALSE;
	
	/* Data buffer. */
	char data[CHUNK_SIZE];
	size_t data_len;
	
	/* Read from file with desired transfer mode. */
	size_t readbytes;
	switch (mode) {
	case NetASCII:
		if (FileReadNetASCII(data, CHUNK_SIZE, file, &readbytes, &data_len, breakconn) == FALSE) return FALSE;
		break;
	case Octet:
		data_len = readbytes = fread(data, 1, CHUNK_SIZE, file);
		break;
	default:
		return FALSE;
	}
	
	/* Assure that packet is long enough to fit all data. */
	if (packetsize - sizeof(DATA) < data_len) {
		if (fseek(file, -(long)readbytes, SEEK_CUR) != 0) {
			if (breakconn == NULL) err(1, ERR_PACK_DATA);
			*breakconn = TRUE;
		}
		return FALSE;
	}
	
	/* Set data opcode and block ID. */
	packet->opcode = htons(Data);
	packet->blkid = htons(blkid);
	
	/* Copy data. */
	memcpy(packet->data, data, data_len);
	
	/* Write total amount of bytes. */
	if (totalbytes != NULL) *totalbytes = sizeof(DATA) + data_len;
	
	/* Set if packet is final. */
	if (isfinal != NULL) *isfinal = (data_len < CHUNK_SIZE) ? TRUE : FALSE;
	
	/* Return success. */
	return TRUE;
	
}

/* Unpack acknowledgement packet. */
BOOL unpack_ack_packet(const ACK* packet, size_t packetsize, uint16_t* blkid) {
	
	/* Assure that packet size is correct. */
	if (packetsize != sizeof(ACK)) return FALSE;
	
	/* Assure that opcode is acknowledgement. */
	if (packet->opcode != htons(Acknowledgement)) return FALSE;
	
	/* Set block ID. */
	*blkid = ntohs(packet->blkid);
	
	/* Return success. */
	return TRUE;
	
}

/* Pack acknowledgement packet. */
BOOL pack_ack_packet(ACK* packet, size_t packetsize, uint16_t blkid) {

	/* Assure that packet size is correct. */
	if (packetsize != sizeof(ACK)) return FALSE;
	
	/* Set acknowledgement opcode and block ID. */
	packet->opcode = htons(Acknowledgement);
	packet->blkid = htons(blkid);
	
	/* Return success. */
	return TRUE;

}

/* Unpack error packet. */
BOOL unpack_err_packet(const ERROR* packet, size_t packetsize, ERRCODE* errcode, char* errmsg, size_t nerrmsg) {
	
	/* Assure that packet has its minimum size. */
	if (packetsize < sizeof(ERROR) + 1) return FALSE;
	
	/* Assure that packet has the appropriate opcode set. */
	if (packet->opcode != htons(Error)) return FALSE;
	
	/* Assure that packet ends with null-termination character. */
	const char* data = (const char*)packet;
	if (data[packetsize - 1] != '\0') return FALSE;
	
	/* Fetch error message argument and its length. */
	const char* arg_errmsg = data + sizeof(ERROR);
	size_t arg_errmsg_len = strlen(arg_errmsg);
	
	/* Assure that packet contains no more arguments. */
	if (arg_errmsg_len != packetsize - sizeof(ERROR) - 1) return FALSE;
	
	/* Assure that buffer is long enough to contain error message argument. */
	if (nerrmsg < arg_errmsg_len + 1) return FALSE;
	
	/* Set error code. */
	*errcode = ERRCODE_NTOHS(packet->errcode);
	
	/* Copy error message. */
	memcpy(errmsg, arg_errmsg, arg_errmsg_len + 1);
	
	/* Return success. */
	return TRUE;
	
}

/* Pack error packet. */
BOOL pack_err_packet(ERROR* packet, size_t packetsize, ERRCODE errcode, const char* errmsg, size_t* totalbytes) {
	
	/* Calculate error message length. */
	size_t errmsg_len = strlen(errmsg);
	
	/* Assure that packet is long enough to contain required data. */
	size_t reqbytes = sizeof(ERROR) + errmsg_len + 1;
	if (packetsize < reqbytes) return FALSE;
	
	/* Set opcode and error code. */
	packet->opcode = htons(Error);
	packet->errcode = ERRCODE_HTONS(errcode);
	
	/* Copy error message. */
	memcpy(packet->errmsg, errmsg, errmsg_len + 1);
	
	/* Write total bytes. */
	if (totalbytes != NULL) *totalbytes = reqbytes;
	
	/* Return success. */
	return TRUE;
	
}

/* Check whether path is allowed. */
BOOL path_allowed(const char* path, BOOL* allowed) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(path, WORKDIR, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	return IsPathContained(cpath, WORKDIR, allowed);
}

/* Check whether file exists. */
BOOL file_exists(const char* filename, BOOL* exists) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	*exists = (access(cpath, F_OK) == 0) ? TRUE : FALSE;
	return TRUE;
}

/* Check whether file can be written to. */
BOOL file_writable(const char* filename, BOOL* writable) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	*writable = (access(cpath, W_OK) == 0) ? TRUE : FALSE;
	return TRUE;
}

/* Check whether file can be read from. */
BOOL file_readable(const char* filename, BOOL* readable) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	*readable = (access(cpath, R_OK) == 0) ? TRUE : FALSE;
	return TRUE;
}

/* Check if there is at least minbytes of space in dir directory. */
BOOL dir_enough_space(const char* dir, size_t minbytes, BOOL* isenough) {
	struct statvfs info;
	if (statvfs(dir, &info) != 0) return FALSE;
	*isenough = (info.f_bsize * info.f_bfree >= minbytes) ? TRUE : FALSE;
	return TRUE;
}

/* Create directories recursively. */
BOOL dir_create_recursive(const char* filename) {
	
	/* Copy file path. */
	size_t pathlen = strlen(filename);
	char path[pathlen + 1];
	memcpy(path, filename, pathlen + 1);
	
	/* Create every directory if it doesn't exist already. */
	for (size_t i = 1; i < pathlen; ++i) {
		if (path[i] == '/') {
			path[i] = '\0';
			struct stat sb;
			if ((stat(path, &sb) != 0) || (!S_ISDIR(sb.st_mode))) {
				if (mkdir(path, 0777) != 0) return FALSE;
			}
			path[i] = '/';
		}
	}
	
	/* Exit success. */
	return TRUE;
	
}

/* Open file for writing. */
BOOL open_file_write(const char* filename, FILE** file, ERRCODE* errcode, const char** errmsg) {
	
	/* Canonicalize path. */
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	
	/* Assure that path is allowed. */
	BOOL allowed;
	if (path_allowed(cpath, &allowed) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	if (allowed == FALSE) {
		*errcode = AccessViolation;
		*errmsg = ERRSTR_PATH;
		return FALSE;
	}
	
	/* Assure that file doesn't exist. */
	BOOL exists;
	if (file_exists(cpath, &exists) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	if (exists == TRUE) {
		*errcode = FileAlreadyExists;
		*errmsg = ERRSTR_EXISTS;
		return FALSE;
	}
	
	/* Check if there is minimum amount of space required for a new file. */
	BOOL enough_space;
	if (dir_enough_space(WORKDIR, MIN_SPACE_SIZE, &enough_space) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	if (enough_space == FALSE) {
		*errcode = DiskFull;
		*errmsg = ERRSTR_DISKFULL;
		return FALSE;
	}
	
	/* Recurively create directories. */
	if (dir_create_recursive(cpath) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	
	/* Open file for writing. */
	FILE* fp = fopen(cpath, "w+b");
	if (fp == NULL) {
		*errcode = AccessViolation;
		*errmsg = ERRSTR_CREATION;
		return FALSE;
	}
	
	/* Set file pointer. */
	*file = fp;
	
	/* Return success. */
	return TRUE;
	
}

/* Open file for reading. */
BOOL open_file_read(const char* filename, FILE** file, ERRCODE* errcode, const char** errmsg) {
	
	/* Canonicalize path. */
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	
	/* Assure that path is allowed. */
	BOOL allowed;
	if (path_allowed(cpath, &allowed) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	if (allowed == FALSE) {
		*errcode = AccessViolation;
		*errmsg = ERRSTR_PATH;
		return FALSE;
	}
	
	/* Assure that file exists. */
	BOOL exists;
	if (file_exists(cpath, &exists) == FALSE) {
		*errcode = NotDefined;
		*errmsg = ERRSTR_INTERNAL;
		return FALSE;
	}
	if (exists == FALSE) {
		*errcode = FileNotFound;
		*errmsg = ERRSTR_NOTFOUND;
		return FALSE;
	}
	
	/* Open file for reading. */
	FILE* fp = fopen(cpath, "rb");
	if (fp == NULL) {
		*errcode = AccessViolation;
		*errmsg = ERRSTR_OPEN;
		return FALSE;
	}
	
	/* Set file pointer. */
	*file = fp;
	
	/* Return success. */
	return TRUE;
	
}

/* Close opened file. */
BOOL close_file(FILE* file) {
	return (fclose(file) == 0) ? TRUE : FALSE;
}

/* Close opened file and delete it. */
BOOL close_file_delete(FILE* file, const char* filename) {
	if (close_file(file) == FALSE) return FALSE;
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, WORKDIR, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	return (remove(cpath) == 0) ? TRUE : FALSE;
}
