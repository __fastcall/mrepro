#ifndef __LOG_H__
#define __LOG_H__

/* Shared header. */
#include "shared.h"

/* My identifiers. */
#define USERNAME "aj50009"
#define HEADER "MrePro tftpserver"
#define MESSAGE_LIMIT 1024

/* Initializes process as daemon. */
extern void init_daemon();

/* Prints message to error output(s) as is. */
extern void debug_msg(FILE* where, const char* msg);

/* Prints connection message to error output(s). */
extern void debug_msg_conn(struct sockaddr_in* addr, const char* filename);

/* Prints error packet message to error output(s). */
extern void debug_msg_error(ERRCODE errcode, const char* errmsg);

#endif
