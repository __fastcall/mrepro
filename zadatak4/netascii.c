/* NetASCII header. */
#include "netascii.h"

/* File descriptor set. */
static fd_set na_fds;
static pthread_mutex_t na_lock;

/* Initialize NetASCII context. */
BOOL InitNetASCII() {
	FD_ZERO(&na_fds);
	return (pthread_mutex_init(&na_lock, NULL) == 0) ? TRUE : FALSE;
}

/* Deinitialize NetASCII context. */
BOOL DeinitNetASCII() {
	return (pthread_mutex_destroy(&na_lock) == 0) ? TRUE : FALSE;
}

/* Write NetASCII content to file. */
BOOL FileWriteNetASCII(const char* buff, size_t bufflen, FILE* file, size_t* writtenbytes, BOOL* breakconn) {

	/* Set break connection to false. */
	if (breakconn != NULL) *breakconn = FALSE;

	/* If buffer is empty nothing needs to be done. */
	if (bufflen == 0) {
		if (writtenbytes != NULL) *writtenbytes = 0;
		return TRUE;
	}
	
	/* If the current character is LF, find out if previous one was CR. */
	if (buff[0] == '\n') {
		long fpos;
		if ((fpos = ftell(file)) == -1) return FALSE;
		if (fpos > 0) {
			if (fseek(file, -1, SEEK_CUR) != 0) return FALSE;
			int c = fgetc(file);
			if (c == EOF) { if (breakconn == NULL) err(1, ERR_WRITE_NETASCII); *breakconn = TRUE; return FALSE; }
			if (c == '\r') {
				if (fseek(file, -1, SEEK_CUR) != 0) return FALSE;
				if (fputc('\n', file) == EOF) { if (breakconn == NULL) err(1, ERR_WRITE_NETASCII); *breakconn = TRUE; return FALSE; }
				buff++; bufflen--;
			}
		}
	}
	
	/* If no more bytes are remaining in the buffer nothing needs to be done. */
	if (bufflen == 0) {
		if (writtenbytes != NULL) *writtenbytes = 0;
		return TRUE;
	}
	
	/* Iterate trough each byte and write to file, whilst converting CR+LF to just LF. */
	size_t count = 0;
	int i;
	for (i = 0; i < bufflen - 1; ++i) {
		if ((buff[i] == '\r') && (buff[i + 1] == '\n')) continue;
		if (fputc(buff[i], file) == EOF) { if (breakconn == NULL) err(1, ERR_WRITE_NETASCII); *breakconn = TRUE; return FALSE; }
		count++;
	}
	
	/* Write last character. */
	if (fputc(buff[i], file) == EOF) { if (breakconn == NULL) err(1, ERR_WRITE_NETASCII); *breakconn = TRUE; return FALSE; }
	count++;
	
	/* Set writtenbytes. */
	if (writtenbytes != NULL) *writtenbytes = count;
	
	/* Exit success. */
	return TRUE;
	
}

/* Read NetASCII content from file. */
BOOL FileReadNetASCII(char* buff, size_t bufflen, FILE* file, size_t* readbytes, size_t* outputbytes, BOOL* breakconn) {

	/* Set break connection to false. */
	if (breakconn != NULL) *breakconn = FALSE;
	
	/* If buffer is empty nothing needs to be done. */
	if (bufflen == 0) {
		if (readbytes != NULL) *readbytes = 0;
		if (outputbytes != NULL) *outputbytes = 0;
		return TRUE;
	}
	
	/* Tell file position. */
	long fpos;
	if ((fpos = ftell(file)) == -1) return FALSE;

	/* Output bytes counter. */
	size_t outcount = 0;

	/* Check file descriptor flag and clear it afterwards. */
	if (pthread_mutex_lock(&na_lock) != 0) return FALSE;
	if (fpos > 0) {
		if (FD_ISSET(fileno(file), &na_fds)) { *buff++ = '\n'; bufflen--; outcount++; }
	}
	FD_CLR(fileno(file), &na_fds);
	if (pthread_mutex_unlock(&na_lock) != 0) err(1, ERR_DEADLOCK); /* unrecoverable */

	/* If no more bytes are remaining in the buffer nothing needs to be done. */
	if (bufflen == 0) {
		if (readbytes != NULL) *readbytes = 0;
		if (outputbytes != NULL) *outputbytes = outcount;
		return TRUE;
	}
	
	/* Iterate trough each byte and write to buffer, whilst converting LF to CR+LF. */
	size_t readcount = 0;
	int i;
	for (i = 0; i < bufflen - 1; ++i) {
		int c = fgetc(file);
		if (c == EOF) {
			if (feof(file)) break;
			if (breakconn == NULL) err(1, ERR_READ_NETASCII);
			*breakconn = TRUE;
			return FALSE;
		}
		readcount++;
		if (c == '\n') {
			buff[i++] = '\r';
			outcount++;
		}
		buff[i] = c;
		outcount++;
	}
	
	/* Write last character. */
	if (i == bufflen - 1) {
		int c = fgetc(file);
		if (c == EOF) {
			if (!feof(file)) {
				if (breakconn == NULL) err(1, ERR_READ_NETASCII);
				*breakconn = TRUE;
				return FALSE;
			}
		} else {
			readcount++;
			if (c == '\n') {
				buff[i++] = '\r';
				if (pthread_mutex_lock(&na_lock) != 0) { if (breakconn == NULL) err(1, ERR_READ_NETASCII); *breakconn = TRUE; return FALSE; }
				FD_SET(fileno(file), &na_fds);
				if (pthread_mutex_unlock(&na_lock) != 0) err(1, ERR_DEADLOCK); /* unrecoverable */
			} else {
				buff[i++] = c;
			}
			outcount++;
		}
	}
	
	/* Set readbytes and outputbytes. */
	if (readbytes != NULL) *readbytes = readcount;
	if (outputbytes != NULL) *outputbytes = outcount;
	
	/* Exit success. */
	return TRUE;

}
