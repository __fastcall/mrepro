/* Log header. */
#include "log.h"

/* Is process a daemon? */
static BOOL is_daemon = FALSE;

/* Initialize process as daemon. */
void init_daemon() {
	
	/* Set as daemon. */
	is_daemon = TRUE;
	
	/* Fork and close parent. */
	pid_t pid = fork();
	if (pid < 0)
		errx(1, "Internal error");
	else if (pid != 0)
		exit(0);
		
	/* Set as session leader. */
	if (setsid() < 0) errx(1, "Internal error");
	
	/* Ignore hang up signal. */
	signal(SIGHUP, SIG_IGN);
	
	/* Fork and close parent. */
	if (pid < 0)
		errx(1, "Internal error");
	else if (pid != 0)
		exit(0);
		
	/* Change working directory to root. */
	chdir("/");
	
	/* Close all streams. */
	fcloseall();
	
	/* Redirect stdin, stdout and stderr to /dev/null. */
	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);
	
	/* Open log. */
	openlog(USERNAME ":" HEADER, LOG_PID, LOG_FTP);
	
}

/* Print message to error output(s) as is. */
void debug_msg(FILE* where, const char* msg) {
	
	/* Print to standard error. */
	fprintf(where, "%s\n", msg);
	
	/* Print to syslog if daemon is on. */
	if (is_daemon) {
		syslog(LOG_INFO, "%s", msg);
	}
	
}

/* Print connection message to error output(s). */
void debug_msg_conn(struct sockaddr_in* addr, const char* filename) {
	char addrstr[NTOP_MAXLEN];
	Ntop(&(addr->sin_addr), addrstr, NTOP_MAXLEN);
	char msg[MESSAGE_LIMIT];
	sprintf(msg, "%s->%s", addrstr, filename);
	debug_msg(stdout, msg);
}

/* Print error packet message to error output(s). */
void debug_msg_error(ERRCODE errcode, const char* errmsg) {
	char msg[MESSAGE_LIMIT];
	sprintf(msg, "TFTP ERROR %d %s", errcode, errmsg);
	debug_msg(stderr, msg);
}
