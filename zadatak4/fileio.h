#ifndef __FILEIO_H__
#define __FILEIO_H__

/* Shared header. */
#include "shared.h"

/* Fatal error messages. */
#define ERR_UNPACK_DATA "data unpacking failed"
#define ERR_PACK_DATA "data packing failed"

/* Allowed working directory. */
#define WORKDIR "/tftpboot"

/* Max chunk size. */
#define CHUNK_SIZE 512

/* Minimum amount of space remaining in bytes required to allow transfer of new file. */
#define MIN_SPACE_SIZE 4096

/* Transfer mode strings. */
#define STR_NETASCII "netascii"
#define STR_OCTET "octet"

/* Transfer modes. */
enum TRANSMODE {
	Unknown = 0,
	NetASCII = 1,
	Octet = 2
};
typedef int TRANSMODE;

/* Unpacks read request packet. */
extern BOOL unpack_rrq_packet(const REQUEST* packet, size_t packetsize, char* filename, size_t nfilename, TRANSMODE* mode);

/* Packs read request packet. Argument totalbytes could be set to NULL if result is not important, but it is not recommended. */
extern BOOL pack_rrq_packet(REQUEST* packet, size_t packetsize, const char* filename, TRANSMODE mode, size_t* totalbytes);

/* Unpacks write request packet. */
extern BOOL unpack_wrq_packet(const REQUEST* packet, size_t packetsize, char* filename, size_t nfilename, TRANSMODE* mode);

/* Packs write request packet. Argument totalbytes could be set to NULL if result is not important, but it is not recommended. */
extern BOOL pack_wrq_packet(REQUEST* packet, size_t packetsize, const char* filename, TRANSMODE mode, size_t* totalbytes);

/* Unpacks data packet. Data will be written to file descriptor in desired transfer mode. Argument isfinal can be set to NULL to ignore the result. If breakconn is set to NULL, program will terminate in fatal situations. */
extern BOOL unpack_data_packet(const DATA* packet, size_t packetsize, uint16_t* blkid, FILE* file, TRANSMODE mode, BOOL* isfinal, BOOL* breakconn);

/* Packs data packet. Data will be read from file descriptor in desired transfer mode. Argument totalbytes could be set to NULL if result is not important, but it is not recommended. Argument isfinal can be set to NULL to ignore the result. If breakconn is set to NULL, program will terminate in fatal situations. */
extern BOOL pack_data_packet(DATA* packet, size_t packetsize, uint16_t blkid, FILE* file, TRANSMODE mode, size_t* totalbytes, BOOL* isfinal, BOOL* breakconn);

/* Unpacks acknowledgement packet. */
extern BOOL unpack_ack_packet(const ACK* packet, size_t packetsize, uint16_t* blkid);

/* Packs acknowledgement packet. */
extern BOOL pack_ack_packet(ACK* packet, size_t packetsize, uint16_t blkid);

/* Unpacks error packet. */
extern BOOL unpack_err_packet(const ERROR* packet, size_t packetsize, ERRCODE* errcode, char* errmsg, size_t nerrmsg);

/* Packs error packet. Argument totalbytes could be set to NULL if result is not important, but it is not recommended. */
extern BOOL pack_err_packet(ERROR* packet, size_t packetsize, ERRCODE errcode, const char* errmsg, size_t* totalbytes);

/* Checks whether path is allowed. */
extern BOOL path_allowed(const char* path, BOOL* allowed);

/* Checks whether file exists. */
extern BOOL file_exists(const char* filename, BOOL* exists);

/* Checks whether file can be written to. */
extern BOOL file_writable(const char* filename, BOOL* writable);

/* Checks whether file can be read from. */
extern BOOL file_readable(const char* filename, BOOL* readable);

/* Checks if there is at least minbytes of space in dir directory. */
extern BOOL dir_enough_space(const char* dir, size_t minbytes, BOOL* isenough);

/* Creates directories recursively. */
extern BOOL dir_create_recursive(const char* filename);

/* Opens file for writing. If failed, returns false and outputs error code and error message. */
extern BOOL open_file_write(const char* filename, FILE** file, ERRCODE* errcode, const char** errmsg);

/* Opens file for reading. If failed, returns false and outputs error code and error message. */
extern BOOL open_file_read(const char* filename, FILE** file, ERRCODE* errcode, const char** errmsg);

/* Closes opened file. */
extern BOOL close_file(FILE* file);

/* Closes opened file and deletes it. */
extern BOOL close_file_delete(FILE* file, const char* filename);

#endif
