/* TFTP server header. */
#include "tftpserver.h"

/* NetASCII header. */
#include "netascii.h"

/* Logger header. */
#include "log.h"

/* Global options. */
static struct {
	BOOL is_daemon;
	const char* service;
} global = { FALSE, NULL };

/* Function forward-declarations. */
static BOOL argfunc(int, const char*);
static void handle(const char*, size_t, const struct sockaddr_in*);
static void* handle_async(void*);
static BOOL pack_and_log_err(ERROR*, size_t, ERRCODE, const char*, size_t*);

/* Entry function. */
int main(int argc, char** argv) {
	
	/* Parse command-line arguments. */
	if (Parseargs(argc, argv, "d", argfunc, 1) == FALSE) { fprintf(stderr, "Usage: %s [-d] port_name_or_number\n", argv[0]); return 1; }
	
	/* Initialize NetASCII. */
	if (InitNetASCII() == FALSE) errx(1, "Internal error");
	
	/* Get address info from service. */
	struct sockaddr_in addr;
	GETADDRINFO(NULL, global.service, PROTOCOL_UDP, &addr);
	
	/* Initialize daemon if option is specified. */
	if (global.is_daemon == TRUE) init_daemon();
	
	/* Create main socket. */
	int mainsock;
	CREATEUDPSOCKET2(&addr, &mainsock);
	
	/* Endless serve. */
	while (TRUE) {
		
		/* Receive packet from client. */
		char packet[MAX_PACKET_SIZE];
		size_t packetsize;
		struct sockaddr_in addr;
		if (Recvfrom(mainsock, packet, MAX_PACKET_SIZE, &addr, &packetsize) == TRUE)
			handle(packet, packetsize, &addr);
		
	}
	
	/* Close main socket. */
	Close(mainsock);
	
	/* Close log. */
	if (global.is_daemon == TRUE) closelog();
	
	/* Deinitialize NetASCII. */
	if (DeinitNetASCII() == FALSE) errx(1, "Internal error");
	
	/* Exit success. */
	return 0;
	
}

/* Global option decoder callback function. */
BOOL argfunc(int flag, const char* arg) {
	switch (flag) {
	case 'd': global.is_daemon = TRUE; return TRUE;
	case INDEX_PARAM(0): global.service = arg; return TRUE;
	}
	return FALSE;
}

/* Thread worker parameters. */
typedef struct {
	const char* packet;
	size_t packetsize;
	const struct sockaddr_in* addr;
	BOOL can_free;
} HANDLEPARAMS;

/* Handle request from client. */
void handle(const char* packet, size_t packetsize, const struct sockaddr_in* addr) {

	/* Prepare parameters and thread. */
	HANDLEPARAMS hp = { packet, packetsize, addr, FALSE };
	pthread_t thread;
	
	/* Create and start thread. */
	if (pthread_create(&thread, NULL, handle_async, &hp) != 0) return;
	
	/* Wait for thread to copy parameters. */
	while (hp.can_free == FALSE) { }

}

/* Thread worker function. */
void* handle_async(void* params) {

	/* Declare parameters. */
	char filename[PATH_MAXLEN];
	TRANSMODE transmode;
	HANDLEPARAMS* hp = (HANDLEPARAMS*)params;
	
	/* Copy socket address. */
	struct sockaddr_in addr;
	memcpy(&addr, hp->addr, sizeof(struct sockaddr_in));
	
	/* Create communication socket. */
	int commsock;
	if (Socket(TYPE_DATAGRAM, PROTOCOL_UDP, &commsock) == FALSE) { hp->can_free = TRUE; return NULL; }
	if (SetSockOpt_RcvTimeo(commsock, 1, 0) == FALSE) { Close(commsock); hp->can_free = TRUE; return NULL; }
	
	/* Declare packet. */
	char packet[MAX_PACKET_SIZE];
	size_t packetsize;
	
	/* Decode request packet. */
	if (unpack_rrq_packet((REQUEST*)hp->packet, hp->packetsize, filename, PATH_MAXLEN, &transmode) == TRUE) {
		
		/* Release resource "lock". */
		hp->can_free = TRUE;
		
		/* Print connection message. */
		debug_msg_conn(&addr, filename);
		
		/* Declare file pointer, error code and message. */
		FILE* file;
		ERRCODE errcode;
		const char* errmsg;
		
		/* Open file for reading. */
		if (open_file_read(filename, &file, &errcode, &errmsg) == TRUE) {
			
			/* Iterate for each block ID. */
			for (uint16_t blkid = 1; TRUE; ++blkid) {
				
				/* Control booleans. */
				BOOL isfinal, breakcomm;
				
				/* Pack data packet. */
				if (pack_data_packet((DATA*)packet, MAX_PACKET_SIZE, blkid, file, transmode, &packetsize, &isfinal, &breakcomm) == FALSE) {
					if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, NotDefined, ERRMSG_STR_INTERNAL, &packetsize) == TRUE)
						Sendto(commsock, packet, packetsize, &addr, NULL);
					break;
				}
				
				/* Repeat for max retransmission amounts until confirmation is received. */
				int i;
				for (i = 0; i < MAX_RETRANSMISSION; ++i) {
					
					/* Try to send data packet. */
					Sendto(commsock, packet, packetsize, &addr, NULL);
					
					/* Try to receive acknowledgement. */
					ACK ack;
					size_t acklen;
					sap_fix:
					if (Recvfrom(commsock, &ack, sizeof(ACK), &addr, &acklen) == TRUE) {
						uint16_t ack_blkid;
						if ((unpack_ack_packet(&ack, acklen, &ack_blkid) == FALSE) || (ack_blkid > blkid)) {
							if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, IllegalTFTPOperation, ERRMSG_STR_INVALIDACK, &packetsize) == TRUE)
								Sendto(commsock, packet, packetsize, &addr, NULL);
							close_file(file);
							Close(commsock);
							return NULL;
						}
						if (ack_blkid == blkid) break;
						goto sap_fix;
					}
					
				}
				
				/* If exceeded maximum amount of attempts, send error and break. */
				if (i == MAX_RETRANSMISSION) {
					if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, NotDefined, ERRMSG_STR_RETRANSMISSION, &packetsize) == TRUE)
						Sendto(commsock, packet, packetsize, &addr, NULL);
					break;
				}
				
				/* Break sending if it is final. */
				if (isfinal == TRUE) break;
				
			}
			
			/* Closes file. */
			close_file(file);
			
		} else {
			
			/* Pack, log and send error message. */
			if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, errcode, errmsg, &packetsize) == TRUE)
				Sendto(commsock, packet, packetsize, &addr, NULL);
			
		}
		
		
	} else if (unpack_wrq_packet((REQUEST*)hp->packet, hp->packetsize, filename, PATH_MAXLEN, &transmode) == TRUE) {
		
		/* Release resource "lock". */
		hp->can_free = TRUE;

		/* Print connection message. */
		debug_msg_conn(&addr, filename);
		
		/* Declare file pointer, error code and message. */
		FILE* file;
		ERRCODE errcode;
		const char* errmsg;
		
		/* Open file for reading. */
		if (open_file_write(filename, &file, &errcode, &errmsg) == TRUE) {
			
			/* Send accept acknowledgement */
			if (pack_ack_packet((ACK*)packet, sizeof(ACK), 0) == FALSE) {
				if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, NotDefined, ERRMSG_STR_INTERNAL, &packetsize) == TRUE)
					Sendto(commsock, packet, packetsize, &addr, NULL);
				close_file_delete(file, filename);
				Close(commsock);
				return NULL;
			}
			Sendto(commsock, packet, sizeof(ACK), &addr, NULL);
			
			/* Iterate for each block ID. */
			for (uint16_t blkid = 1; TRUE; ++blkid) {
			
				/* Control booleans. */
				BOOL isfinal, breakcomm;
				
				/* Repeat for max retransmission amounts until package is received. */
				int i;
				for (i = 0; i < MAX_RETRANSMISSION; ++i) {
					
					/* Target acknowledgement ID. */
					uint16_t target_blkid = blkid - 1;
					
					/* Attempt to receive packet. */
					if (Recvfrom(commsock, packet, MAX_PACKET_SIZE, &addr, &packetsize) == TRUE) {
						
						/* Validate packet block ID. */
						if ((packetsize < sizeof(DATA)) || (ntohs(((DATA*)packet)->blkid) > blkid)) {
							if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, IllegalTFTPOperation, ERRMSG_STR_INVALIDDATA, &packetsize) == TRUE)
								Sendto(commsock, packet, packetsize, &addr, NULL);
							close_file_delete(file, filename);
							Close(commsock);
							return NULL;
						}
						
						/* Unpack if expected data packet. */
						if (ntohs(((DATA*)packet)->blkid) == blkid) {
							if (unpack_data_packet((DATA*)packet, packetsize, &target_blkid, file, transmode, &isfinal, &breakcomm) == FALSE) {
								if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, IllegalTFTPOperation, ERRMSG_STR_INVALIDDATA, &packetsize) == TRUE)
									Sendto(commsock, packet, packetsize, &addr, NULL);
								close_file_delete(file, filename);
								Close(commsock);
								return NULL;
							}
						} else i--;
						
					} else if (i == MAX_RETRANSMISSION - 1) continue;
					
					/* Send acknowledgement packet. */
					ACK ack;
					if (pack_ack_packet(&ack, sizeof(ACK), target_blkid) == FALSE) {
						if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, NotDefined, ERRMSG_STR_INTERNAL, &packetsize) == TRUE)
							Sendto(commsock, packet, packetsize, &addr, NULL);
						close_file_delete(file, filename);
						Close(commsock);
						return NULL;
					}
					Sendto(commsock, &ack, sizeof(ACK), &addr, NULL);
					
					/* Break if received required block ID data packet. */
					if (target_blkid == blkid) break;
					
				}
				
				/* If exceeded maximum amount of attempts, send error and break. */
				if (i == MAX_RETRANSMISSION) {
					if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, NotDefined, ERRMSG_STR_RETRANSMISSION, &packetsize) == TRUE)
						Sendto(commsock, packet, packetsize, &addr, NULL);
					close_file_delete(file, filename);
					Close(commsock);
					return NULL;
				}
				
				/* Break receiving if it is final. */
				if (isfinal == TRUE) break;
			
			}
			
			/* Closes file. */
			close_file(file);
			
		} else {
			
			/* Pack, log and send error message. */
			if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, errcode, errmsg, &packetsize) == TRUE)
				Sendto(commsock, packet, packetsize, &addr, NULL);
		
		}
		
	} else {
		
		/* Release resource "lock". */
		hp->can_free = TRUE;
		
		/* Pack, log and send error message. */
		if (pack_and_log_err((ERROR*)packet, MAX_PACKET_SIZE, IllegalTFTPOperation, ERRMSG_STR_MALFORMED, &packetsize) == TRUE)
			Sendto(commsock, packet, packetsize, &addr, NULL);
		
	}
	
	/* Close communication socket. */
	Close(commsock);
	
	/* Return ignored. */
	return NULL;
	
}

/* Pack error packet and log error message. */
BOOL pack_and_log_err(ERROR* packet, size_t packetsize, ERRCODE errcode, const char* errmsg, size_t* totalbytes) {
	debug_msg_error(errcode, errmsg);
	return pack_err_packet(packet, packetsize, errcode, errmsg, totalbytes);
}
