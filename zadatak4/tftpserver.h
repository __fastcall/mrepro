#ifndef __TFTPSERVER_H__
#define __TFTPSERVER_H__

/* Shared header. */
#include "shared.h"

/* File IO header. */
#include "fileio.h"

/* Packet really shouldn't be any larger than this. */
#define MAX_PACKET_SIZE (sizeof(DATA) + CHUNK_SIZE)

/* Amount of transmissions before breaking connection. */
#define MAX_RETRANSMISSION 4

/* Error message strings. */
#define ERRMSG_STR_MALFORMED "Malformed packet received"
#define ERRMSG_STR_INTERNAL "Internal server error"
#define ERRMSG_STR_RETRANSMISSION "Exceeded maximum retransmission attempts"
#define ERRMSG_STR_INVALIDACK "Invalid acknowledgement packet"
#define ERRMSG_STR_INVALIDDATA "Invalid data packet"

#endif
