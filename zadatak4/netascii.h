#ifndef __NETASCII_H__
#define __NETASCII_H__

/* Shared header. */
#include "shared.h"

/* Fatal error messages. */
#define ERR_WRITE_NETASCII "netascii write failed"
#define ERR_READ_NETASCII "netascii read failed"
#define ERR_DEADLOCK "unrecoverable deadlock situation"

/* Initializes NetASCII. */
extern BOOL InitNetASCII();

/* Deinitializes NetASCII. */
extern BOOL DeinitNetASCII();

/* Writes NetASCII content to file. Variable writtenbytes can be set to NULL if not important. If breakconn is set to NULL, program will terminate in fatal situations. */
extern BOOL FileWriteNetASCII(const char* buff, size_t bufflen, FILE* file, size_t* writtenbytes, BOOL* breakconn);

/* Reads NetASCII content from file. Variable readbytes and/or outputbytes can be set to NULL if not important. If breakconn is set to NULL, program will terminate in fatal situations. */
extern BOOL FileReadNetASCII(char* buff, size_t bufflen, FILE* file, size_t* readbytes, size_t* outputbytes, BOOL* breakconn);

#endif
