#include <wrappers.h>

#define MAX_IP_UDP_PAIRS 5
#define MAX_TCP_CONNS 2
#define MAX_STR_COMMAND_BUFFER 512
#define MAX_STR_DATA_BUFFER 512

typedef struct {
	const char* ip;
	const char* udp;
	struct sockaddr_in addr;
	int sock;
	int data;
	BOOL updated;
} IpUdpPair;

static const char* g_TcpPort = "1234";
static const char* g_UdpPort = "1234";
static const char* g_Seconds = "5";
static long g_SecondsNumber;
static IpUdpPair g_IpUdpPairs[MAX_IP_UDP_PAIRS];
static int g_Count;
static int g_ClientSock = -1;
static struct sockaddr_in g_ClientAddr;

static int sock_tcp, sock_udp;
static struct sockaddr_in addr_tcp, addr_udp;

static void prnconnmsg(const char* prefix, struct sockaddr_in* addr) {
	char ipaddr[NTOP_MAXLEN];
	NTOP(&(addr->sin_addr), ipaddr, NTOP_MAXLEN);
	int port = ntohs(addr->sin_port);
	fprintf(stderr, "%s: %s:%i\n", prefix, ipaddr, port);
}

static BOOL flagfunc(int flag, const char* arg) {
	switch (flag) {
	case 't':
		g_TcpPort = arg;
		return TRUE;
	case 'u':
		g_UdpPort = arg;
		return TRUE;
	case 's':
		g_Seconds = arg;
		return TRUE;
	}
	if (flag >= INDEX_PARAM(2 * MAX_IP_UDP_PAIRS)) return FALSE;
	int idx = (flag - ADDITIONAL_PARAM) / 2;
	int mod = (flag - ADDITIONAL_PARAM) % 2;
	if (mod == 0) {
		g_IpUdpPairs[idx].ip = arg;
	} else {
		g_IpUdpPairs[idx].udp = arg;
	}
	g_Count = idx + 1;
	return TRUE;
}

#define MOD_STDIO 0
#define MOD_TCP 1
#define MOD_UDP 2
#define IDX_GET(x) ((x) - 1)
#define SENSOR_VALID(x) (((x) >= 0) && ((x) < g_Count))
#define RESP_ERR (-1)

static void respond(int mod, int ans) {
	char data[MAX_STR_DATA_BUFFER + 1];
	if (ans == RESP_ERR) {
		sprintf(data, "%s\n", "ERR");
	} else {
		sprintf(data, "%i\n", ans);
	}
	switch (mod) {
	case MOD_STDIO:
		fprintf(stdout, "%s", data);
		break;
	case MOD_TCP:
		if (g_ClientSock != -1) {
			SENDN(g_ClientSock, data, strlen(data));
		}
		break;
	case MOD_UDP:
		SENDTO(sock_udp, data, strlen(data), &addr_udp, NULL);
		break;
	}
}

// #define LOG_ALL

#ifdef LOG_ALL
#define LOG_fprnf(a,b,c) fprintf(a,b,c)
#else
#define LOG_fprnf(a,b,c)
#endif

static void prncmd(char* command, int mod) {
	switch (mod) {
	case MOD_STDIO:
		LOG_fprnf(stderr, "STDIN Naredba: %s", command);
		break;
	case MOD_TCP:
		fprintf(stderr, "Naredba: %s", command);
		break;
	case MOD_UDP:
		LOG_fprnf(stderr, "UDP Naredba: %s", command);
		break;
	}
}

static void exec_cmd(char* command, int mod) {
	prncmd(command, mod);
	if (strcmp(command, "status\n") == 0) {
		respond(mod, g_Count);
	} else if (strcmp(command, "avg\n") == 0) {
		int argval = 0;
		for (int i = 0; i < g_Count; ++i) {
			if (!g_IpUdpPairs[i].updated) {
				respond(mod, RESP_ERR);
				return;
			}
			argval += g_IpUdpPairs[i].data;
		}
		argval /= g_Count;
		respond(mod, argval);
	} else if (strcmp(command, "reset\n") == 0) {
		for (int i = 0; i < g_Count; ++i) {
			CLOSE(g_IpUdpPairs[i].sock);
			SOCKET(TYPE_DATAGRAM, PROTOCOL_UDP, &(g_IpUdpPairs[i].sock));
			SETSOCKOPT_RCVTIMEO(g_IpUdpPairs[i].sock, g_SecondsNumber, 0);
			SENDTO(g_IpUdpPairs[i].sock, "INIT\n", 5, &(g_IpUdpPairs[i].addr), NULL);
			g_IpUdpPairs[i].data = 0;
			g_IpUdpPairs[i].updated = FALSE;
		}
	} else if (strlen(command) == 7) {
		char charbkp = command[4];
		command[4] = '\0';
		int idx = IDX_GET(strtol(command + 5, NULL, 10));
		if (SENSOR_VALID(idx)) {
			if (strcmp(command, "data") == 0) {
				SENDTO(g_IpUdpPairs[idx].sock, "STAT\n", 5, &(g_IpUdpPairs[idx].addr), NULL);
				char data[MAX_STR_DATA_BUFFER + 1];
				size_t recvbytes;
				if (Recvfrom(g_IpUdpPairs[idx].sock, data, MAX_STR_DATA_BUFFER, &(g_IpUdpPairs[idx].addr), &recvbytes) == FALSE) {
					respond(mod, RESP_ERR);
					return;
				}
				data[recvbytes] = '\0';
				g_IpUdpPairs[idx].data = strtol(data, NULL, 10);
				g_IpUdpPairs[idx].updated = TRUE;
				respond(mod, g_IpUdpPairs[idx].data);
			} else if (strcmp(command, "last") == 0) {
				if (!g_IpUdpPairs[idx].updated) {
					respond(mod, RESP_ERR);
					return;
				}
				respond(mod, g_IpUdpPairs[idx].data);
			} else {
				command[4] = charbkp;
				respond(mod, RESP_ERR);
			}
		} else {
			respond(mod, RESP_ERR);
		}
	} else {
		respond(mod, RESP_ERR);
	}
}

int main(int argc, char** argv) {

	/* Parse arguments. */
	if (Parseargs(argc, argv, "t:u:s:", flagfunc, AT_LEAST_ADDITIONAL_PARAMS(2)) == FALSE) {
		errx(1, "at least 1 and at most 5 IoT devices must be provided");
	}

	/* Parse TCP and UDP addresses. */
	GETADDRINFO(NULL, g_TcpPort, PROTOCOL_TCP, &addr_tcp);
	GETADDRINFO(NULL, g_UdpPort, PROTOCOL_UDP, &addr_udp);

	/* Create TCP and UDP sockets. */
	CREATETCPSOCKET(NULL, g_TcpPort, MAX_TCP_CONNS, &sock_tcp);
	CREATEUDPSOCKET(NULL, g_UdpPort, &sock_udp);
	
	/* Parse seconds argument. */
	g_SecondsNumber = strtol(g_Seconds, NULL, 10);
	if ((g_SecondsNumber == 0) && ((errno == EINVAL) || (errno == ERANGE))) {
		errx(1, "provided argument \"%s\" for -s is not a number", g_Seconds);
	}
	
	/* Connect to all UDP sockets. */
	for (int i = 0; i < g_Count; ++i) {
		GETADDRINFO(g_IpUdpPairs[i].ip, g_IpUdpPairs[i].udp, PROTOCOL_UDP, &(g_IpUdpPairs[i].addr));
		SOCKET(TYPE_DATAGRAM, PROTOCOL_UDP, &(g_IpUdpPairs[i].sock));
		SETSOCKOPT_RCVTIMEO(g_IpUdpPairs[i].sock, g_SecondsNumber, 0);
		SENDTO(g_IpUdpPairs[i].sock, "INIT\n", 5, &(g_IpUdpPairs[i].addr), NULL);
		g_IpUdpPairs[i].data = 0;
		g_IpUdpPairs[i].updated = FALSE;
	}

	/* Endless loop. */
	while (TRUE) {
		
		/* Set required file descriptor flags. */
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(STDIN, &fds);
		FD_SET(sock_tcp, &fds);
		FD_SET(sock_udp, &fds);
		int fds_count = sock_udp + 1;
		for (int i = 0; i < g_Count; ++i) {
			if (g_IpUdpPairs[i].sock + 1 > fds_count)
				fds_count = g_IpUdpPairs[i].sock + 1;
			FD_SET(g_IpUdpPairs[i].sock, &fds);
		}
		if (g_ClientSock != -1) {
			if (g_ClientSock + 1 > fds_count)
				fds_count = g_ClientSock + 1;
			FD_SET(g_ClientSock, &fds);
		}
		
		/* Select available socket. */
		SELECT(fds_count, &fds, NULL, NULL);
		
		/* Read STDIN input if available. */
		if (FD_ISSET(STDIN, &fds)) {
			char command[MAX_STR_COMMAND_BUFFER];
			fgets(command, MAX_STR_COMMAND_BUFFER, stdin);
			exec_cmd(command, MOD_STDIO);
		}
		
		/* Read TCP input if available. */
		if (FD_ISSET(sock_tcp, &fds)) {
			if (g_ClientSock == -1) {
				ACCEPT(sock_tcp, &g_ClientAddr, &g_ClientSock);
				prnconnmsg("Spojio se", &g_ClientAddr);
			} else {
				int tmp_sock;
				struct sockaddr_in tmp_addr;
				ACCEPT(sock_tcp, &tmp_addr, &tmp_sock);
				const char* errmsg = "Dozvoljen samo jedan TCP klijent.\n";
				SENDN(tmp_sock, (char*)errmsg, strlen(errmsg));
				CLOSE(tmp_sock);
			}
		}
		
		/* Read UDP input if available. */
		if (FD_ISSET(sock_udp, &fds)) {
			char command[MAX_STR_COMMAND_BUFFER + 1];
			size_t recvbytes;
			RECVFROM(sock_udp, command, MAX_STR_COMMAND_BUFFER, &addr_udp, &recvbytes);
			command[recvbytes] = '\0';
			exec_cmd(command, MOD_UDP);
		}
		
		/* Read from all sensors. */
		for (int i = 0; i < g_Count; ++i) {
			if (FD_ISSET(g_IpUdpPairs[i].sock, &fds)) {
				char data[MAX_STR_DATA_BUFFER + 1];
				size_t recvbytes;
				RECVFROM(g_IpUdpPairs[i].sock, data, MAX_STR_DATA_BUFFER, &(g_IpUdpPairs[i].addr), &recvbytes);
				data[recvbytes] = '\0';
				g_IpUdpPairs[i].data = strtol(data, NULL, 10);
				g_IpUdpPairs[i].updated = TRUE;
			}
		}
		
		/* Read from TCP client. */
		if ((g_ClientSock != -1) && (FD_ISSET(g_ClientSock, &fds))) {
			char command[MAX_STR_COMMAND_BUFFER + 1];
			size_t recvbytes;
			BOOL isEOF;
			RECVUNTIL(g_ClientSock, command, MAX_STR_COMMAND_BUFFER, '\n', &recvbytes, &isEOF);
			if (isEOF) {
				prnconnmsg("Odspojio se", &g_ClientAddr);
				CLOSE(g_ClientSock);
				g_ClientSock = -1;
			} else {
				command[recvbytes] = '\0';
				exec_cmd(command, MOD_TCP);
			}
		}
		
	}

	return 0;
	
}
