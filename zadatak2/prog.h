#ifndef __PROG_H__
#define __PROG_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>

/* Common macros. */
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

/* Common error messages. */
#define ERRMSG_USAGE "prog [-r] [-t|-u] [-x] [-h|-n] [-46] {hostname|IP_address} {servicename|port}\n"
#define ERRMSG_EXARGS "mutally exclusive arguments -- %c and %c"
#define ERRMSG_IPADDR "invalid IP address format"
#define ERRMSG_PORTNUM "invalid port number format"
#define ERRMSG_NORESLT "no results found"
#define ERRMSG_NOTIMPL "(debug) unimplemented %s value %#x"

/* Transport layer protocol. */
enum trans_layer_proto {
	TLP_TCP = 0x1,
	TLP_UDP = 0x2
};

/* Byte order. */
enum byte_order {
	BO_HOST = 0x1,
	BO_NETWORK = 0x2
};

/* IP address mode. */
enum ip_mode {
	IPM_IPV4 = 0x1,
	IPM_IPV6 = 0x2
};

/* Program state. */
struct prog_state {
	int tlp, hex, bo, ipm, rev;
};

#endif
