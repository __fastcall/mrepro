#include "prog.h"

/* Program state. */
static struct prog_state state = { 0 };

/* Entry main function. */
int main(int argc, char** argv) {

	/* Decode command-line options. */
	int opt;
	while ((opt = getopt(argc, argv, "tuxhn46r")) != -1) {
		switch (opt) {
		case 't': state.tlp |= TLP_TCP; break;
		case 'u': state.tlp |= TLP_UDP; break;
		case 'x': state.hex = 1; break;
		case 'h': state.bo |= BO_HOST; break;
		case 'n': state.bo |= BO_NETWORK; break;
		case '4': state.ipm |= IPM_IPV4; break;
		case '6': state.ipm |= IPM_IPV6; break;
		case 'r': state.rev = 1; break;
		default: fprintf(stderr, ERRMSG_USAGE); return 1;
		}
	}
	
	/* Check if required amount of parameters is provided. */
	if (argc - optind != 2) {
		fprintf(stderr, ERRMSG_USAGE);
		return 1;
	}

	/* Set default options if unspecified. */
	if (!state.tlp) state.tlp = TLP_TCP;
	if (!state.bo) state.bo = BO_HOST;
	if (!state.ipm) state.ipm = IPM_IPV4;
	
	/* Check if mutually exclusive arguments are specified simultaneously. */
	if (state.tlp == (TLP_TCP | TLP_UDP))
		errx(1, ERRMSG_EXARGS, 't', 'u');
	else if (state.bo == (BO_HOST | BO_NETWORK))
		errx(1, ERRMSG_EXARGS, 'h', 'n');
	
	/* Check if reverse lookup. */
	if (state.rev) {
		
		/* Check if IP is set to IPv4 and IPv6 simultaneously. */
		if (state.ipm == (IPM_IPV4 | IPM_IPV6))
			errx(1, ERRMSG_EXARGS, '4', '6');
	
		/* Fetch remaining arguments. */
		const char* ipaddr = argv[argc - 2];
		const char* port = argv[argc - 1];
		unsigned short portnum;
		
		/* Parse port number. */
		if ((portnum = strtol(port, NULL, (state.hex ? 16 : 10))) == 0)
			errx(2, ERRMSG_PORTNUM);
		if (state.bo == BO_HOST)
			portnum = htons(portnum);
		
		/* Socket address info. */
		struct sockaddr_in6 saddr = { 0 };
		
		/* Parse IP address and enter port number. */
		if (state.ipm == IPM_IPV4) {
			struct sockaddr_in* saddr_in = (struct sockaddr_in*)&saddr;
			saddr_in->sin_family = AF_INET;
			if (inet_pton(AF_INET, ipaddr, &(saddr_in->sin_addr)) != 1)
				errx(2, ERRMSG_IPADDR);
			saddr_in->sin_port = portnum;
		} else if (state.ipm == IPM_IPV6) {
			struct sockaddr_in6* saddr_in6 = (struct sockaddr_in6*)&saddr;
			saddr_in6->sin6_family = AF_INET6;
			if (inet_pton(AF_INET6, ipaddr, &(saddr_in6->sin6_addr)) != 1)
				errx(2, ERRMSG_IPADDR);
			saddr_in6->sin6_port = portnum;
		} else errx(2, ERRMSG_NOTIMPL, "ip_mode", state.ipm);
	
		/* Resulting buffers. */
		char hostname[NI_MAXHOST];
		char servicename[NI_MAXSERV];
		int r;
		
		/* Get name info. */
		if ((r = getnameinfo((struct sockaddr*)&saddr, (state.ipm == IPM_IPV4) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6), hostname, sizeof(hostname), servicename, sizeof(servicename), ((state.tlp == TLP_UDP) ? NI_DGRAM : 0)))) {
			errx(2, "%s", gai_strerror(r));
		}
		
		/* Print result. */
		printf("%s (%s) %s\n", ipaddr, hostname, servicename);
	
	} else {
		
		/* Fetch remaining arguments. */
		const char* hostname = argv[argc - 2];
		const char* servicename = argv[argc - 1];
		
		/* Construct address info hints. */
		struct addrinfo hints = {
			AI_CANONNAME,
			AF_INET,
			0,
			((state.tlp == TLP_TCP) ? IPPROTO_TCP : 0) | ((state.tlp == TLP_UDP) ? IPPROTO_UDP : 0)
		};
		struct addrinfo hints6 = {
			AI_CANONNAME,
			AF_INET6,
			0,
			hints.ai_protocol
		};
		
		/* Get address info linked list. */
		int r, count = 0;
		struct addrinfo* llist = NULL;
		struct addrinfo* llist6 = NULL;
		if ((state.ipm & IPM_IPV4) && (r = getaddrinfo(hostname, servicename, &hints, &llist)))
			errx(2, "%s", gai_strerror(r));
		if ((state.ipm & IPM_IPV6) && (r = getaddrinfo(hostname, servicename, &hints6, &llist6)))
			errx(2, "%s", gai_strerror(r));
		
		/* Print all IPv4 results. */
		for (struct addrinfo* node = llist; node; node = node->ai_next) {
			
			/* Found domain canonical name. */
			const char* cname = node->ai_canonname;
			
			/* Convert numeric address to presentation and find port number. */
			char ipaddr[INET_ADDRSTRLEN];
			struct sockaddr_in* addr_in = (struct sockaddr_in*)node->ai_addr;
			inet_ntop(AF_INET, &(addr_in->sin_addr), ipaddr, sizeof(ipaddr));
			unsigned short port = (state.bo == BO_HOST) ? ntohs(addr_in->sin_port) : addr_in->sin_port;
			
			/* Print result. */
			if (state.hex)
				printf("%s (%s) %04x\n", ipaddr, cname, port);
			else
				printf("%s (%s) %i\n", ipaddr, cname, port);
				
			/* Increment result count. */
			count++;
			
		}
		
		/* Print all IPv6 results. */
		for (struct addrinfo* node = llist6; node; node = node->ai_next) {
			
			/* Found domain canonical name. */
			const char* cname = node->ai_canonname;
			
			/* Convert numeric address to presentation and find port number. */
			char ipaddr[INET6_ADDRSTRLEN];
			struct sockaddr_in6* addr_in6 = (struct sockaddr_in6*)node->ai_addr;
			inet_ntop(AF_INET6, &(addr_in6->sin6_addr), ipaddr, sizeof(ipaddr));
			unsigned short port = (state.bo == BO_HOST) ? ntohs(addr_in6->sin6_port) : addr_in6->sin6_port;
			
			/* Print result. */
			if (state.hex)
				printf("%s (%s) %04x\n", ipaddr, cname, port);
			else
				printf("%s (%s) %i\n", ipaddr, cname, port);
				
			/* Increment result count. */
			count++;
			
		}
		
		/* Clean address info linked lists. */
		if (llist) freeaddrinfo(llist);
		if (llist6) freeaddrinfo(llist6);
		
		/* Check if any results were found. */
		if (count == 0) errx(3, ERRMSG_NORESLT);
		
	}

	return 0;

}
