#include "http.h"

/* Debugging stats. */
#ifdef HTTP_DEBUG

#define DBG(a, m) (dbg(a, m))

static void dbg(const struct sockaddr_in* addr, const char* msg) {
	char buff[NTOP_MAXLEN];
	if (Ntop((struct in_addr*)&addr->sin_addr, buff, NTOP_MAXLEN) == FALSE) return;
	printf("HTTP: %s:%i -- %s\n", buff, ntohs(addr->sin_port), msg);
}

#else

#define DBG(a, m)

#endif

/* Supported HTTP request method strings. */
const char* HttpMethodStrings[] = {
	"GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "OPTIONS", "CONNECT", "PATCH"
};

/* Known file mime types. */
const char* HttpMimeTypePairs[] = {
	"html", "text/html; charset=utf-8",
	"htm", "text/html; charset=utf-8",
	"css", "text/css; charset=utf-8",
	"js", "text/javascript; charset=utf-8",
	"json", "application/json; charset=utf-8",
	"xml", "application/xml; charset=utf-8",
	"txt", "text/plain; charset=utf-8",
	"jpeg", "image/jpeg",
	"jpg", "image/jpeg",
	"gif", "image/gif",
	"png", "image/png",
	"bmp", "image/bmp",
	"tiff", "image/tiff",
	"tif", "image/tiff",
	"ico", "image/vnd.microsoft.icon",
	"svg", "image/svg+xml",
	"webp", "image/webp",
	"pdf", "application/pdf",
	NULL, "application/octet-stream"
};

/* (Internal use only) Initialize HTTP server. */
static void _init_http_server(RequestFunc fallback, HttpServer* httpserver) {
	httpserver->timeout = HTTP_DEFAULT_TIMEOUT;
	httpserver->hsr.head = httpserver->hsr.tail = NULL;
	httpserver->clients.head = httpserver->clients.tail = NULL;
	httpserver->fallback = fallback;
}

/* Create a HTTP server from hostname and service. */
BOOL CreateHttpServer(const char* hostname, const char* service, int backlog, RequestFunc fallback, HttpServer* result) {
	if (CreateTcpSocket(hostname, service, backlog, &(result->sock)) == FALSE) return FALSE;
	_init_http_server(fallback, result);
	return TRUE;
}

/* Create a HTTP server from socket address. */
BOOL CreateHttpServer2(struct sockaddr_in* addr, int backlog, RequestFunc fallback, HttpServer* result) {
	if (CreateTcpSocket2(addr, backlog, &(result->sock)) == FALSE) return FALSE;
	_init_http_server(fallback, result);
	return TRUE;
}

/* (Internal use only) Construct HTTP serving rule node. */
static HsrNode* _construct_hsr_node(const HttpServingRule* rule) {
	HsrNode* hsr = (HsrNode*)malloc(sizeof(HsrNode));
	if (hsr == NULL) return NULL;
	memcpy(&(hsr->rule), rule, sizeof(HttpServingRule));
	if (CanonicalizePath(rule->path, "/", hsr->rule.path, PATH_MAXLEN) == FALSE) { free(hsr); return NULL; }
	hsr->prevnode = hsr->nextnode = NULL;
	return hsr;
}

/* (Internal use only) Free HTTP serving rule node. */
static void _free_hsr_node(HsrNode* node) {
	free(node);
}

/* Add a HTTP serving rule at head position. */
BOOL AddHttpServingRuleHead(HttpServer* server, const HttpServingRule* rule) {
	HsrNode* hsr = _construct_hsr_node(rule);
	if (hsr == NULL) return FALSE;
	if (server->hsr.head == NULL) {
		server->hsr.tail = hsr;
	} else {
		server->hsr.head->prevnode = hsr;
		hsr->nextnode = server->hsr.head;
	}
	server->hsr.head = hsr;
	return TRUE;
}

/* Add a HTTP serving rule at tail position. */
BOOL AddHttpServingRuleTail(HttpServer* server, const HttpServingRule* rule) {
	HsrNode* hsr = _construct_hsr_node(rule);
	if (hsr == NULL) return FALSE;
	if (server->hsr.tail == NULL) {
		server->hsr.head = hsr;
	} else {
		server->hsr.tail->nextnode = hsr;
		hsr->prevnode = server->hsr.tail;
	}
	server->hsr.tail = hsr;
	return TRUE;
}

/* Serve HTTP requests with responses. */
BOOL ServeHttp(HttpServer* server) {
	return ServeHttpAlongside(server, 0, NULL, NULL, NULL);
}

/* Serve HTTP requests with responses. */
BOOL ServeHttp_Timeout(HttpServer* server, long sec, long usec) {
	return ServeHttpAlongside_Timeout(server, 0, NULL, NULL, NULL, sec, usec);
}

/* (Internal use only) Construct client socket node. */
static SockNode* _construct_sock_node(HttpServer* server, int sock, const struct sockaddr_in* addr) {
	SockNode* node = (SockNode*)malloc(sizeof(SockNode));
	if (node == NULL) return NULL;
	node->sock = sock;
	memcpy(&(node->addr), addr, sizeof(struct sockaddr_in));
	node->killafter = (unsigned long long)time(NULL) + server->timeout;
	node->bufflen = 0;
	memset(&(node->request), 0, sizeof(HttpRequestData));
	node->request.sock = sock;
	memcpy(&(node->request.addr), addr, sizeof(struct sockaddr_in));
	node->data_mode = FALSE;
	node->prevnode = node->nextnode = NULL;
	return node;
}

/* (Internal use only) Drop client socket node. */
static BOOL _drop_sock_node(HttpServer* server, SockNode* sock) {
	SockNode* nextsock = sock->nextnode;
	SockNode* prevsock = sock->prevnode;
	if (nextsock == NULL) {
		if (prevsock == NULL) {
			server->clients.head = server->clients.tail = NULL;
		} else {
			prevsock->nextnode = NULL;
			server->clients.tail = prevsock;
		}
	} else if (prevsock == NULL) {
		nextsock->prevnode = NULL;
		server->clients.head = nextsock;
	} else {
		prevsock->nextnode = nextsock;
		nextsock->prevnode = prevsock;
	}
	if (Close(sock->sock) == FALSE) return FALSE;
	return TRUE;
}

/* (Internal use only) Free client socket node. */
static void _free_sock_node(SockNode* node) {
	free(node);
}

/* (Internal use only) Decide HTTP response. */
static BOOL _decide_response(HttpServer* server, SockNode* sock, BOOL* safe) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(sock->request.path, "/", cpath, PATH_MAXLEN) == FALSE) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (internal failure)");
		_free_sock_node(sock);
		return FALSE;
	}
	char cpath2[PATH_MAXLEN];
	size_t len = strlen(cpath);
	if (cpath[len - 1] == '/') {
		memcpy(cpath2, cpath, len - 1);
		cpath2[len - 1] = '\0';
	} else if (len + 2 > PATH_MAXLEN) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (internal failure)");
		_free_sock_node(sock);
		return FALSE;
	} else {
		memcpy(cpath2, cpath, len);
		cpath2[len] = '/';
		cpath2[len + 1] = '\0';
	}
	for (HsrNode* node = server->hsr.head; node != NULL; node = node->nextnode) {
		if (node->rule.method_bits & (1 << sock->request.method)) {
			BOOL match = FALSE, succ = TRUE;
			if (node->rule.ignore_case) {
				succ = CompareStrings_IgnoreCase(node->rule.path, cpath, &match);
				if (match == FALSE)
					succ = CompareStrings_IgnoreCase(node->rule.path, cpath2, &match);
				if ((match == FALSE) && (node->rule.serve_subpath))
					succ = IsPathContained_IgnoreCase(cpath, node->rule.path, &match);
			} else {
				match = (strcmp(node->rule.path, cpath) == 0) ? TRUE : FALSE;
				if (match == FALSE)
					match = (strcmp(node->rule.path, cpath2) == 0) ? TRUE : FALSE;
				if ((match == FALSE) && (node->rule.serve_subpath))
					succ = IsPathContained(cpath, node->rule.path, &match);
			}
			if (succ == FALSE) {
				if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
				DBG(&(sock->addr), "disconnect (internal failure)");
				_free_sock_node(sock);
				return FALSE;
			}
			if (match == TRUE) {
				if (node->rule.callback(&(sock->request)) == FALSE) {
					if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
					DBG(&(sock->addr), "disconnect (user function)");
					_free_sock_node(sock);
					return FALSE;
				}
				return TRUE;
			}
		}
	}
	if ((server->fallback == NULL) || (server->fallback(&(sock->request)) == FALSE)) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (user function)");
		_free_sock_node(sock);
		return FALSE;
	}
	return TRUE;
}

/* (Internal use only) Parse HTTP request. */
static BOOL _parse_request(HttpServer* server, SockNode* sock, BOOL* safe) {
	char* begin = sock->buff;
	char* end = strstr(begin, "\r\n");
	*end = '\0'; end += 2;
	char* beginpath = strchr(begin, ' ');
	if (beginpath == NULL) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (malformed request)");
		_free_sock_node(sock);
		return FALSE;
	}
	*beginpath++ = '\0';
	BOOL requestFound = FALSE;
	for (HttpMethod i = 0; i < _HttpMethodCount; ++i) {
		if (strcmp(begin, HttpMethodStrings[i]) == 0) {
			sock->request.method = i;
			requestFound = TRUE;
			break;
		}
	}
	if (requestFound == FALSE) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (unsupported method)");
		_free_sock_node(sock);
		return FALSE;
	}
	char* beginversion = strchr(beginpath, ' ');
	if (beginversion == NULL) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (malformed request)");
		_free_sock_node(sock);
		return FALSE;
	}
	*beginversion++ = '\0';
	size_t pathlen = strlen(beginpath);
	if (pathlen + 1 > PATH_MAXLEN) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (URI too long)");
		_free_sock_node(sock);
		return FALSE;
	}
	memcpy(sock->request.path, beginpath, pathlen + 1);
	if (strcmp(beginversion, "HTTP/1.1") == 0) {
		sock->request.http11 = TRUE;
	} else if (strcmp(beginversion, "HTTP/1.0") != 0) {
		if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
		DBG(&(sock->addr), "disconnect (unsupported version)");
		_free_sock_node(sock);
		return FALSE;
	}
	if ((end[0] == '\r') && (end[1] == '\n')) return TRUE;
	char* final = strstr(end, "\r\n\r\n");
	final[2] = '\0';
	for (begin = end, end = strstr(begin, "\r\n"); end != NULL; begin = end, end = strstr(begin, "\r\n")) {
		*end = '\0'; end += 2;
		char* value = strstr(begin, ": ");
		if (value == NULL) {
			if (_drop_sock_node(server, sock) == FALSE) { *safe = FALSE; return FALSE; }
			DBG(&(sock->addr), "disconnect (malformed request)");
			_free_sock_node(sock);
			return FALSE;
		}
		*value = '\0'; value += 2;
		if (strcmp(begin, "User-Agent") == 0) {
			strncpy(sock->request.user_agent, value, ARG_MAXLEN - 1);
		} else if (strcmp(begin, "Host") == 0) {
			strncpy(sock->request.host, value, ARG_MAXLEN - 1);
		} else if (strcmp(begin, "Content-Type") == 0) {
			strncpy(sock->request.content_type, value, ARG_MAXLEN - 1);
		} else if (strcmp(begin, "Content-Length") == 0) {
			sock->request.content_length = (size_t)strtol(value, NULL, 10);
		} else if (strcmp(begin, "Accept-Language") == 0) {
			strncpy(sock->request.accept_language, value, ARG_MAXLEN - 1);
		} else if (strcmp(begin, "Accept-Encoding") == 0) {
			strncpy(sock->request.accept_encoding, value, ARG_MAXLEN - 1);
		} else if (strcmp(begin, "Connection") == 0) {
			strncpy(sock->request.connection, value, ARG_MAXLEN - 1);
		}
	}
	return TRUE;
}

/* (Internal use only) Serve HTTP socket. */
static BOOL _serve_sock(HttpServer* server, SockNode* sock) {
	size_t recvbytes;
	BOOL isEOF;
	BOOL safe = TRUE;
	if (sock->data_mode) {
		if (Read(sock->sock, sock->buff + sock->bufflen, sock->data_reqd - sock->bufflen, &recvbytes, &isEOF) == FALSE) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "socket read error");
			_free_sock_node(sock);
			return TRUE;
		}
		if (isEOF) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "disconnect (manual)");
			_free_sock_node(sock);
			return TRUE;
		}
		sock->bufflen += recvbytes;
		if (sock->bufflen >= sock->data_reqd) {
			sock->bufflen = 0;
			sock->data_mode = FALSE;
			memcpy(sock->request.data, sock->buff + sock->data_reqd - sock->request.content_length, sock->request.content_length);
			if (_decide_response(server, sock, &safe) == FALSE) return safe;
		}
	} else {
		if (Read(sock->sock, sock->buff + sock->bufflen, CLIENT_BUFFER_SIZE - sock->bufflen, &recvbytes, &isEOF) == FALSE) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "socket read error");
			_free_sock_node(sock);
			return TRUE;
		}
		if (isEOF) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "disconnect (manual)");
			_free_sock_node(sock);
			return TRUE;
		}
		sock->bufflen += recvbytes;
		const char* breakpt;
		if ((breakpt = strnstr(sock->buff, "\r\n\r\n", sock->bufflen)) != NULL) {
			if (_parse_request(server, sock, &safe) == FALSE) return safe;
			sock->data_reqd = ((breakpt + 4) - sock->buff) + sock->request.content_length;
			if (sock->data_reqd > CLIENT_BUFFER_SIZE) {
				if (_drop_sock_node(server, sock) == FALSE) return FALSE;
				DBG(&(sock->addr), "disconnect (too long)");
				_free_sock_node(sock);
				return TRUE;
			}
			if (sock->bufflen >= sock->data_reqd) {
				sock->bufflen = 0;
				memcpy(sock->request.data, sock->buff + sock->data_reqd - sock->request.content_length, sock->request.content_length);
				if (_decide_response(server, sock, &safe) == FALSE) return safe;
				goto serve_skipall;
			} else {
				sock->data_mode = TRUE;
			}
		}
		if (sock->bufflen == CLIENT_BUFFER_SIZE) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "disconnect (too long or malformed request)");
			_free_sock_node(sock);
			return TRUE;
		}
	}
serve_skipall:
	sock->killafter = (unsigned long long)time(NULL) + server->timeout;
	return TRUE;
}

/* (Internal use only) Serve HTTP requests with responses alongside custom file descriptors. */
static BOOL _serve_http_alongside(HttpServer* server, int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, struct timeval* tv) {
	fd_set intfds;
	if (readfds == NULL) readfds = &intfds;
	for (SockNode *sock = server->clients.head, *nextsock; sock != NULL; sock = nextsock) {
		nextsock = sock->nextnode;
		if ((unsigned long long)time(NULL) > sock->killafter) {
			if (_drop_sock_node(server, sock) == FALSE) return FALSE;
			DBG(&(sock->addr), "disconnect (timeout)");
			_free_sock_node(sock);
		} else {
			FD_SET(sock->sock, readfds);
			if (sock->sock + 1 > nfds) nfds = sock->sock + 1;
		}
	}
	FD_SET(server->sock, readfds);
	if (server->sock + 1 > nfds) nfds = server->sock + 1;
	if (select(nfds, readfds, writefds, exceptfds, tv) == -1) return FALSE;
	for (SockNode* sock = server->clients.head; sock != NULL; sock = sock->nextnode) {
		if (FD_ISSET(sock->sock, readfds)) {
			FD_CLR(sock->sock, readfds);
			if (_serve_sock(server, sock) == FALSE) return FALSE;
		}
	}
	if (FD_ISSET(server->sock, readfds)) {
		FD_CLR(server->sock, readfds);
		int clisock;
		struct sockaddr_in cliaddr;
		if (Accept(server->sock, &cliaddr, &clisock) == FALSE) return FALSE;
		SockNode* node = _construct_sock_node(server, clisock, &cliaddr);
		if (node == NULL) return FALSE;
		DBG(&cliaddr, "connect");
		if (server->clients.tail == NULL) {
			server->clients.head = node;
		} else {
			server->clients.tail->nextnode = node;
			node->prevnode = server->clients.tail;
		}
		server->clients.tail = node;
	}
	return TRUE;
}

/* Serve HTTP requests with responses alongside custom file descriptors. */
BOOL ServeHttpAlongside(HttpServer* server, int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds) {
	return _serve_http_alongside(server, nfds, readfds, writefds, exceptfds, NULL);
}

/* Serve HTTP requests with responses alongside custom file descriptors. */
BOOL ServeHttpAlongside_Timeout(HttpServer* server, int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, long sec, long usec) {
	struct timeval tv = { sec, usec };
	return _serve_http_alongside(server, nfds, readfds, writefds, exceptfds, &tv);
}

/* Fill the default response structure data. */
BOOL FillDefaultResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status) {
	memset(response, 0, sizeof(HttpResponseData));
	response->http11 = request->http11;
	response->code = code;
	strncpy(response->status, status, ARG_MAXLEN - 1);
	strncpy(response->server, SERVER_NAME, ARG_MAXLEN - 1);
	strcpy(response->connection, "Keep-Alive");
	sprintf(response->keep_alive, "timeout=%llu", server->timeout);
	return TRUE;
}

/* Fill the HTML response structure data. */
BOOL FillHtmlResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* html) {
	if (FillDefaultResponse(server, request, response, code, status) == FALSE) return FALSE;
	response->content_length = strlen(html);
	strcpy(response->content_type, "text/html");
	response->data = html;
	return TRUE;
}

/* Fill the file response structure data. */
BOOL FillFileResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* filename, const char* mimetype) {
	FILE* f = fopen(filename, "rb");
	if (f == NULL) return FALSE;
	if (fseek(f, 0, SEEK_END) == -1) { fclose(f); return FALSE; }
	long flen = ftell(f);
	fclose(f);
	if (flen == -1) return FALSE;
	if (FillDefaultResponse(server, request, response, code, status) == FALSE) return FALSE;
	response->content_length = flen;
	strcpy(response->content_type, mimetype);
	response->_indirect_file = TRUE;
	strcpy(response->_filename, filename);
	return TRUE;
}

/* Fill the file response structure data found in filesystem directory. */
BOOL FillFilesystemResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* filename, const char* path, const char** mimepairs, BOOL* found) {
	char cpath[PATH_MAXLEN];
	if (CanonicalizePath(filename, path, cpath, PATH_MAXLEN) == FALSE) return FALSE;
	BOOL contained;
	if (IsPathContained(cpath, (path == NULL) ? "." : path, &contained) == FALSE) return FALSE;
	if (contained == FALSE) { if (found == NULL) return FALSE; *found = FALSE; return TRUE; }
	if (access(cpath, F_OK | R_OK) == -1) { if (found == NULL) return FALSE; *found = FALSE; return TRUE; }
	struct stat st;
	if (stat(cpath, &st) == -1) return FALSE;
	if (S_ISDIR(st.st_mode)) { if (found == NULL) return FALSE; *found = FALSE; return TRUE; }
	const char* ext;
	for (ext = cpath + strlen(cpath) - 1; (*ext != '.') && (ext > cpath); ext--);
	if (*ext == '.') ext++;
	if (mimepairs == NULL) mimepairs = HttpMimeTypePairs;
	const char* mimetype;
	BOOL foundmime = FALSE;
	int i;
	for (i = 0; mimepairs[i] != NULL; i += 2) {
		if (CompareStrings_IgnoreCase(ext, mimepairs[i], &foundmime) == FALSE) return FALSE;
		if (foundmime == TRUE) {
			mimetype = mimepairs[i + 1];
			break;
		}
	}
	if (foundmime == FALSE) mimetype = mimepairs[i + 1];
	if (mimetype == NULL) mimetype = "";
	if (FillFileResponse(server, request, response, code, status, cpath, mimetype) == FALSE) return FALSE;
	if (found != NULL) *found = TRUE;
	return TRUE;
}

/* Send HTTP response to client. */
BOOL SendResponse(int sock, const HttpResponseData* response) {
	char buff[CLIENT_BUFFER_SIZE];
	char* output = buff;
	output += sprintf(output, "%s %i %s\r\n", ((response->http11 == TRUE) ? "HTTP/1.1" : "HTTP/1.0"), response->code, response->status);
	if (response->server[0] != '\0')
		output += sprintf(output, "Server: %s\r\n", response->server);
	output += sprintf(output, "Content-Length: %i\r\n", response->content_length);
	if (response->content_type[0] != '\0')
		output += sprintf(output, "Content-Type: %s\r\n", response->content_type);
	if (response->connection[0] != '\0')
		output += sprintf(output, "Connection: %s\r\n", response->connection);
	if (response->keep_alive[0] != '\0')
		output += sprintf(output, "Keep-Alive: %s\r\n\r\n", response->keep_alive);
	size_t len = output - buff;
	if (Sendn(sock, buff, len) == FALSE) return FALSE;
	if (response->content_length != 0) {
		if (response->_indirect_file == TRUE) {
			FILE* f = fopen(response->_filename, "rb");
			if (f == NULL) return FALSE;
			for (size_t l = fread(buff, 1, CLIENT_BUFFER_SIZE, f); l > 0; l = fread(buff, 1, CLIENT_BUFFER_SIZE, f))
				if (Sendn(sock, buff, l) == FALSE) { fclose(f); return FALSE; }
			fclose(f);
		} else return Sendn(sock, (char*)response->data, response->content_length);
	}
	return TRUE;
}

/* Destroy a HTTP server including all native handles. */
BOOL DestroyHttpServer(HttpServer* server) {
	for (HsrNode *hsr = server->hsr.head, *nextnode; hsr != NULL; hsr = nextnode) {
		nextnode = hsr->nextnode;
		_free_hsr_node(hsr);
	}
	for (SockNode *sock = server->clients.head, *nextsock; sock != NULL; sock = nextsock) {
		nextsock = sock->nextnode;
		if (Close(sock->sock) == FALSE) return FALSE;
		_free_sock_node(sock);
	}
	return Close(server->sock);
}
