#ifndef __HTTP_H__
#define __HTTP_H__

/**
 *    Simplistic HTTP server by Antonio J.
 *         (made for MrePro class)
 */

/* Include wrapper definitions. */
#include "wrappers.h"

/* Default HTTP connection timeout (in seconds). */
#define HTTP_DEFAULT_TIMEOUT 30

/* Client buffer maximum length. */
#define CLIENT_BUFFER_SIZE (8 * 1024)

/* Max argument length. */
#define ARG_MAXLEN 256

/* Used by default response. */
#define SERVER_NAME "MrePro Simple HTTP Server"

/* (Debug) Define only if you want to print connection stats. */
#define HTTP_DEBUG

/* Supported HTTP request methods. */
typedef uint32_t HttpMethod;
enum HttpMethod {
	HttpGet = 0,					/* GET */
	HttpHead = 1,					/* HEAD */
	HttpPost = 2,					/* POST */
	HttpPut = 3,					/* PUT */
	HttpDelete = 4,					/* DELETE */
	HttpTrace = 5,					/* TRACE */
	HttpOptions = 6,				/* OPTIONS */
	HttpConnect = 7,				/* CONNECT */
	HttpPatch = 8,					/* PATCH */
	_HttpMethodCount = 9
};

/* Supported HTTP request method bits. */
typedef uint32_t HttpMethodMask;
enum HttpMethodMask {
	HttpMaskGet = (1 << HttpGet),
	HttpMaskHead = (1 << HttpHead),
	HttpMaskPost = (1 << HttpPost),
	HttpMaskPut = (1 << HttpPut),
	HttpMaskDelete = (1 << HttpDelete),
	HttpMaskTrace = (1 << HttpTrace),
	HttpMaskOptions = (1 << HttpOptions),
	HttpMaskConnect = (1 << HttpConnect),
	HttpMaskPatch = (1 << HttpPatch),
	HttpMaskAll = ((1 << _HttpMethodCount) - 1)
};

/* Supported HTTP request method strings. */
extern const char* HttpMethodStrings[];

/* Known file mime types. */
extern const char* HttpMimeTypePairs[];

/* HTTP request data. */
typedef struct {
	int sock;
	struct sockaddr_in addr;
	HttpMethod method;
	char path[PATH_MAXLEN];
	BOOL http11;
	char user_agent[ARG_MAXLEN];
	char host[ARG_MAXLEN];
	char content_type[ARG_MAXLEN];
	size_t content_length;
	char accept_language[ARG_MAXLEN];
	char accept_encoding[ARG_MAXLEN];
	char connection[ARG_MAXLEN];
	char data[CLIENT_BUFFER_SIZE];
} HttpRequestData;

/* HTTP response data. */
typedef struct {
	BOOL http11;
	int code;
	char status[ARG_MAXLEN];
	char server[ARG_MAXLEN];
	size_t content_length;
	char content_type[ARG_MAXLEN];
	char connection[ARG_MAXLEN];
	char keep_alive[ARG_MAXLEN];
	const char* data;
	BOOL _indirect_file;
	char _filename[PATH_MAXLEN];
} HttpResponseData;

/* Request callback function type. */
typedef BOOL (*RequestFunc)(const HttpRequestData* request);

/* HTTP serving rule. */
typedef struct {
	char path[PATH_MAXLEN];
	BOOL ignore_case;
	BOOL serve_subpath;
	HttpMethodMask method_bits;
	RequestFunc callback;
} HttpServingRule;

/* HTTP serving rule node. */
struct _HsrNode {
	HttpServingRule rule;
	struct _HsrNode* prevnode;
	struct _HsrNode* nextnode;
};
typedef struct _HsrNode HsrNode;

/* HTTP client socket node. */
struct _SockNode {
	int sock;
	struct sockaddr_in addr;
	unsigned long long killafter;
	char buff[CLIENT_BUFFER_SIZE];
	size_t bufflen;
	HttpRequestData request;
	BOOL data_mode;
	size_t data_reqd;
	struct _SockNode* prevnode;
	struct _SockNode* nextnode;
};
typedef struct _SockNode SockNode;

/* HTTP server. */
typedef struct {
	int sock;
	unsigned long long timeout;
	struct {
		HsrNode* head;
		HsrNode* tail;
	} hsr;
	struct {
		SockNode* head;
		SockNode* tail;
	} clients;
	RequestFunc fallback;
} HttpServer;

/* Creates a HTTP server from hostname and service. Option SO_REUSEADDR is automatically set upon creation. */
extern BOOL CreateHttpServer(const char* hostname, const char* service, int backlog, RequestFunc fallback, HttpServer* result);
#define CREATEHTTPSERVER(...) (_EXIT_ON_FALSE(CreateHttpServer, ##__VA_ARGS__))

/* Creates a HTTP server from socket address. Option SO_REUSEADDR is automatically set upon creation. */
extern BOOL CreateHttpServer2(struct sockaddr_in* addr, int backlog, RequestFunc fallback, HttpServer* result);
#define CREATEHTTPSERVER2(...) (_EXIT_ON_FALSE(CreateHttpServer2, ##__VA_ARGS__))

/* Adds a HTTP serving rule at head position. Structure HttpServingRule is copied internally. */
extern BOOL AddHttpServingRuleHead(HttpServer* server, const HttpServingRule* rule);
#define ADDHTTPSERVINGRULEHEAD(...) (_EXIT_ON_FALSE(AddHttpServingRuleHead, ##__VA_ARGS__))

/* Adds a HTTP serving rule at tail position. Structure HttpServingRule is copied internally. */
extern BOOL AddHttpServingRuleTail(HttpServer* server, const HttpServingRule* rule);
#define ADDHTTPSERVINGRULETAIL(...) (_EXIT_ON_FALSE(AddHttpServingRuleTail, ##__VA_ARGS__))

/* Serves HTTP requests with responses. Blocks until at least one service is completed. */
extern BOOL ServeHttp(HttpServer* server); /* If this function fails it is recommended to terminate the program. */
#define SERVEHTTP(...) (_EXIT_ON_FALSE(ServeHttp, ##__VA_ARGS__))

/* Serves HTTP requests with responses. Blocks until at least one service is completed or timeout expires (returns TRUE in both cases). */
extern BOOL ServeHttp_Timeout(HttpServer* server, long sec, long usec); /* If this function fails it is recommended to terminate the program. */
#define SERVEHTTP_TIMEOUT(...) (_EXIT_ON_FALSE(ServeHttp_Timeout, ##__VA_ARGS__))

/* Serves HTTP requests with responses alongside custom file descriptors. Blocks until at least one service is completed or at least one file descriptor becomes ready. */
extern BOOL ServeHttpAlongside(HttpServer* server, int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds); /* If this function fails it is recommended to terminate the program. */
#define SERVEHTTPALONGSIDE(...) (_EXIT_ON_FALSE(ServeHttpAlongside, ##__VA_ARGS__))

/* Serves HTTP requests with responses alongside custom file descriptors. Blocks until at least one service is completed, at least one file descriptor becomes ready or timeout expires (returns TRUE in all three cases). */
extern BOOL ServeHttpAlongside_Timeout(HttpServer* server, int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, long sec, long usec); /* If this function fails it is recommended to terminate the program. */
#define SERVEHTTPALONGSIDE_TIMEOUT(...) (_EXIT_ON_FALSE(ServeHttpAlongside_Timeout, ##__VA_ARGS__))

/* Fills the default response structure data. */
extern BOOL FillDefaultResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status);
#define FILLDEFAULTRESPONSE(...) (_EXIT_ON_FALSE(FillDefaultResponse, ##__VA_ARGS__))

/* Fills the HTML response structure data. */
extern BOOL FillHtmlResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* html);
#define FILLHTMLRESPONSE(...) (_EXIT_ON_FALSE(FillHtmlResponse, ##__VA_ARGS__))

/* Fills the file response structure data. */
extern BOOL FillFileResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* filename, const char* mimetype);
#define FILLFILERESPONSE(...) (_EXIT_ON_FALSE(FillFileResponse, ##__VA_ARGS__))

/* Fills the file response structure data found in filesystem directory. Setting NULL on path will use the working directory. Setting NULL on mimepairs will use the default mime pairs. Setting NULL on found will ignore the parameter. */
extern BOOL FillFilesystemResponse(const HttpServer* server, const HttpRequestData* request, HttpResponseData* response, int code, const char* status, const char* filename, const char* path, const char** mimepairs, BOOL* found);
#define FILLFILESYSTEMRESPONSE(...) (_EXIT_ON_FALSE(FillFilesystemResponse, ##__VA_ARGS__))

/* Sends HTTP response to client. */
extern BOOL SendResponse(int sock, const HttpResponseData* response);
#define SENDRESPONSE(...) (_EXIT_ON_FALSE(SendResponse, ##__VA_ARGS__))

/* Destroys a HTTP server including all native handles. */
extern BOOL DestroyHttpServer(HttpServer* server); /* If this function fails it is recommended to terminate the program. */
#define DESTROYHTTPSERVER(...) (_EXIT_ON_FALSE(DestroyHttpServer, ##__VA_ARGS__))

#endif
