#include "tcpklijent.h"

/* Server endpoint and options. */
const char* g_Server = "127.0.0.1";
const char* g_Port = "1234";
BOOL g_Continue = FALSE;
const char* g_Filename = NULL;

BOOL argfunc(int flag, const char* value) {
	
	/* Decode argument. */
	switch (flag) {
	case 's':
		g_Server = value;
		break;
	case 'p':
		g_Port = value;
		break;
	case 'c':
		g_Continue = TRUE;
		break;
	case ADDITIONAL_PARAM:
		g_Filename = value;
		break;
	default:
		return FALSE;
	}
	return TRUE;
	
}

int main(int argc, char** argv) {
	
	/* Parse arguments. */
	if (Parseargs(argc, argv, "s:p:c", argfunc, 1) == FALSE) {
		fprintf(stderr, "Usage: ./tcpklijent [-s server] [-p port] [-c] filename\n");
		return 1;
	}
	
	/* Check if file exists. */
	if (!g_Continue && access(g_Filename, F_OK) == 0) errx(1, "File already exists");
	if (g_Continue && access(g_Filename, F_OK) != 0) errx(1, "File doesn't exist");
	
	/* Resolve hostname and port service. */
	struct sockaddr_in addr;
	GETADDRINFO(g_Server, g_Port, PROTOCOL_TCP, &addr);
	
	/* Create socket and connect to TCP listener. */
	int sock;
	CREATECONNECTSOCKET(TYPE_STREAM, PROTOCOL_TCP, &addr, &sock);
	SETSOCKOPT_REUSEADDR(sock, TRUE);
	
	/* If file is not continued. */
	if (!g_Continue) {
		
		/* Send file offset. */
		size_t nooffset = htonl(nooffset);
		SENDN(sock, &nooffset, sizeof(nooffset));
					
		/* Send file name. */
		SENDN(sock, (char*)g_Filename, strlen(g_Filename) + 1);
		
		/* Receive status. */
		unsigned char status;
		RECVN(sock, &status, 1);
					
		/* Check status is OK. */
		if (status != 0) {
			char buff[CHUNK_SIZE];
			size_t recv;
			if (Recvuntil(sock, buff, sizeof(buff), '\0', &recv) == FALSE) errx(status, "Unknown error");
			buff[recv] = '\0';
			errx(status, "%s", buff);
		}
		
	}
	
	/* Open file. */
	FILE* f = fopen(g_Filename, g_Continue ? "ab" : "wb");
	if (f != NULL) {
		
		/* Seek file end. */
		if (fseek(f, 0, SEEK_END) == 0) {
			
			/* Get file offset. */
			size_t offset = ftell(f);
			if (offset != -1) {
				
				/* If file is continued. */
				if (g_Continue) {
					
					/* Send file offset. */
					offset = htonl(offset);
					SENDN(sock, &offset, sizeof(offset));
					
					/* Send file name. */
					SENDN(sock, (char*)g_Filename, strlen(g_Filename) + 1);
					
					/* Receive status. */
					unsigned char status;
					RECVN(sock, &status, 1);
					
					/* Check status is OK. */
					if (status != 0) {
						char buff[CHUNK_SIZE];
						size_t recv;
						if (Recvuntil(sock, buff, sizeof(buff), '\0', &recv) == FALSE) errx(status, "Unknown error");
						buff[recv] = '\0';
						errx(status, "%s", buff);
					}
				
				}
				
				/* Write to end. */
				while (TRUE) {
					char buff[CHUNK_SIZE];
					size_t recv = 0;
					BOOL isEOF = FALSE;
					READ(sock, buff, sizeof(buff), &recv, &isEOF);
					fwrite(buff, 1, recv, f);
					if (isEOF) break;
				}
				
				/* Close file. */
				fclose(f);
				
			} else { fclose(f); Close(sock); errx(1, "File offset could not be told");} 
			
		} else { fclose(f); Close(sock); errx(1, "File cannot be seeked"); }
		
	} else { Close(sock); errx(1, "File cannot be opened"); }
	
	/* Close socket. */
	Close(sock);
	
	return 0;
	
}
