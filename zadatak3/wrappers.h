#ifndef __WRAPPERS_H__
#define __WRAPPERS_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* Don't use directly. */
#ifdef DEBUG
#define _EXIT_ON_FALSE(fcall, ...) {if (fcall(__VA_ARGS__) == FALSE) errx(1, "(%s:%d) %s -- %s", __FILE__, __LINE__, #fcall "(" #__VA_ARGS__ ")", (errno == 0) ? "unknown error" : strerror(errno));}
#define _EXIT_ON_FALSE_GAI(fcall, ...) {if (fcall(__VA_ARGS__) == FALSE) errx(1, "(%s:%d) %s -- %s", __FILE__, __LINE__, #fcall "(" #__VA_ARGS__ ")", (_gai_errno == 0) ? "unknown error" : gai_strerror(_gai_errno));}
#else
#define _EXIT_ON_FALSE(fcall, ...) {if (fcall(__VA_ARGS__) == FALSE) errx(1, "%s -- %s", #fcall, (errno == 0) ? "unknown error" : strerror(errno));}
#define _EXIT_ON_FALSE_GAI(fcall, ...) {if (fcall(__VA_ARGS__) == FALSE) errx(1, "%s -- %s", #fcall, (_gai_errno == 0) ? "unknown error" : gai_strerror(_gai_errno));}
#endif
extern int _gai_errno; /* not thread safe! */

/* Boolean, true when function succeeds, false otherwise. */
typedef int BOOL;
enum BOOL { FALSE = 0, TRUE = 1 };

/* Starting value for additional param. */
#define ADDITIONAL_PARAM 256

/* Parses command line arguments. Calls flagfunc(...) for every recognized argument. For additional parameters, arguments will start from ADDITIONAL_PARAM. */
extern BOOL Parseargs(int argc, char* const argv[], const char* validflags, BOOL (*flagfunc)(int flag, const char* arg), int additionalparams);
#define PARSEARGS(...) (_EXIT_ON_FALSE(Parseargs, ##__VA_ARGS__))

/* Maximum IPv4 presentation string length (including NULL-ternimator). */
#define NTOP_MAXLEN INET_ADDRSTRLEN

/* IPv4 address network to presentation. For guaranteed success, assure bufflen is at least NTOP_MAXLEN. */
extern BOOL Ntop(struct in_addr* addr, char* buff, size_t bufflen);
#define NTOP(...) (_EXIT_ON_FALSE(Ntop, ##__VA_ARGS__))

/* Maximum IPv6 presentation string length (including NULL-ternimator). */
#define NTOP6_MAXLEN INET6_ADDRSTRLEN

/* IPv6 address network to presentation. For guaranteed success, assure bufflen is at least NTOP6_MAXLEN. */
extern BOOL Ntop6(struct in6_addr* addr, char* buff, size_t bufflen);
#define NTOP6(...) (_EXIT_ON_FALSE(Ntop6, ##__VA_ARGS__))

/* Presentation to network IPv4 address. */
extern BOOL Pton(char* presentation, struct in_addr* addr);
#define PTON(...) (_EXIT_ON_FALSE(Pton, ##__VA_ARGS__))

/* Presentation to network IPv6 address. */
extern BOOL Pton6(char* presentation, struct in6_addr* addr);
#define PTON6(...) (_EXIT_ON_FALSE(Pton6, ##__VA_ARGS__))

/* Easier to remember, protocols for Getaddrinfo wrappers. */
typedef int PROTOCOL;
enum PROTOCOL { PROTOCOL_ANY = 0, PROTOCOL_UDP = IPPROTO_UDP, PROTOCOL_TCP = IPPROTO_TCP };

/* Get IPv4 address info. AI_PASSIVE flag set automatically if hostname==NULL. */
extern BOOL Getaddrinfo(const char* hostname, const char* service, PROTOCOL protocol, struct sockaddr_in* result);
#define GETADDRINFO(...) (_EXIT_ON_FALSE_GAI(Getaddrinfo, ##__VA_ARGS__))

/* Get IPv6 address info. AI_PASSIVE flag set automatically if hostname==NULL. */
extern BOOL Getaddrinfo6(const char* hostname, const char* service, PROTOCOL protocol, struct sockaddr_in6* result);
#define GETADDRINFO6(...) (_EXIT_ON_FALSE_GAI(Getaddrinfo6, ##__VA_ARGS__))

/* Easier to remember, socket type. */
typedef int TYPE;
enum TYPE { TYPE_DATAGRAM = SOCK_DGRAM, TYPE_STREAM = SOCK_STREAM };

/* Creates IPv4 socket. */
extern BOOL Socket(TYPE type, PROTOCOL protocol, int* result);
#define SOCKET(...) (_EXIT_ON_FALSE(Socket, ##__VA_ARGS__))

/* Creates IPv6 socket. */
extern BOOL Socket6(TYPE type, PROTOCOL protocol, int* result);
#define SOCKET6(...) (_EXIT_ON_FALSE(Socket6, ##__VA_ARGS__))

/* Closes a socket. */
extern BOOL Close(int sock);
#define CLOSE(...) (_EXIT_ON_FALSE(Close, ##__VA_ARGS__))

/* Binds IPv4 socket to address. */
extern BOOL Bind(int sock, struct sockaddr_in* addr);
#define BIND(...) (_EXIT_ON_FALSE(Bind, ##__VA_ARGS__))

/* Binds IPv6 socket to address. */
extern BOOL Bind6(int sock, struct sockaddr_in6* addr);
#define BIND6(...) (_EXIT_ON_FALSE(Bind6, ##__VA_ARGS__))

/* Creates and binds IPv4 socket. */
extern BOOL CreateBindSocket(TYPE type, PROTOCOL protocol, struct sockaddr_in* addr, int* result);
#define CREATEBINDSOCKET(...) (_EXIT_ON_FALSE(CreateBindSocket, ##__VA_ARGS__))

/* Creates and binds IPv6 socket. */
extern BOOL CreateBindSocket6(TYPE type, PROTOCOL protocol, struct sockaddr_in6* addr, int* result);
#define CREATEBINDSOCKET6(...) (_EXIT_ON_FALSE(CreateBindSocket6, ##__VA_ARGS__))

/* Listens on socket (stream/seqpacket). */
extern BOOL Listen(int sock, int maxconn);
#define LISTEN(...) (_EXIT_ON_FALSE(Listen, ##__VA_ARGS__))

/* Accepts IPv4 connection and creates socket with client. If not interested in client address, you can set addr to NULL. */
extern BOOL Accept(int sock, struct sockaddr_in* addr, int* result);
#define ACCEPT(...) (_EXIT_ON_FALSE(Accept, ##__VA_ARGS__))

/* Accepts IPv6 connection and creates socket with client. If not interested in client address, you can set addr to NULL. */
extern BOOL Accept6(int sock, struct sockaddr_in6* addr, int* result);
#define ACCEPT6(...) (_EXIT_ON_FALSE(Accept6, ##__VA_ARGS__))

/* Connects to IPv4 server listener. */
extern BOOL Connect(int sock, struct sockaddr_in* addr);
#define CONNECT(...) (_EXIT_ON_FALSE(Connect, ##__VA_ARGS__))

/* Connects to IPv6 server listener. */
extern BOOL Connect6(int sock, struct sockaddr_in6* addr);
#define CONNECT6(...) (_EXIT_ON_FALSE(Connect6, ##__VA_ARGS__))

/* Creates socket and connects to IPv4 server listener. */
extern BOOL CreateConnectSocket(TYPE type, PROTOCOL protocol, struct sockaddr_in* addr, int* result);
#define CREATECONNECTSOCKET(...) (_EXIT_ON_FALSE(CreateConnectSocket, ##__VA_ARGS__))

/* Creates socket and connects to IPv6 server listener. */
extern BOOL CreateConnectSocket6(TYPE type, PROTOCOL protocol, struct sockaddr_in6* addr, int* result);
#define CREATECONNECTSOCKET6(...) (_EXIT_ON_FALSE(CreateConnectSocket6, ##__VA_ARGS__))

/* Sends a (datagram) packet to IPv4 address via socket. Variable sentbytes can be ignored if set to NULL. */
extern BOOL Sendto(int sock, void* buff, size_t bufflen, struct sockaddr_in* to, size_t* sentbytes);
#define SENDTO(...) (_EXIT_ON_FALSE(Sendto, ##__VA_ARGS__))

/* Sends a (datagram) packet to IPv6 address via socket. Variable sentbytes can be ignored if set to NULL. */
extern BOOL Sendto6(int sock, void* buff, size_t bufflen, struct sockaddr_in6* to, size_t* sentbytes);
#define SENDTO6(...) (_EXIT_ON_FALSE(Sendto6, ##__VA_ARGS__))

/* Receives a (datagram) packet from IPv4 address via socket. Variable recvbytes can be ignored if set to NULL. */
extern BOOL Recvfrom(int sock, void* buff, size_t bufflen, struct sockaddr_in* to, size_t* recvbytes);
#define RECVFROM(...) (_EXIT_ON_FALSE(Recvfrom, ##__VA_ARGS__))

/* Receives a (datagram) packet from IPv6 address via socket. Variable recvbytes can be ignored if set to NULL. */
extern BOOL Recvfrom6(int sock, void* buff, size_t bufflen, struct sockaddr_in6* to, size_t* recvbytes);
#define RECVFROM6(...) (_EXIT_ON_FALSE(Recvfrom6, ##__VA_ARGS__))

/* Writes data to socket stream. Variable sentbytes can be ignored if set to NULL. */
extern BOOL Write(int sock, void* buff, size_t bufflen, size_t* sentbytes);
#define WRITE(...) (_EXIT_ON_FALSE(Write, ##__VA_ARGS__))

/* Reads data from socket stream. Variable recvbytes and/or iseof can be ignored if set to NULL. */
extern BOOL Read(int sock, void* buff, size_t bufflen, size_t* recvbytes, BOOL* isEOF);
#define READ(...) (_EXIT_ON_FALSE(Read, ##__VA_ARGS__))

/* Sends all requested bytes to socket stream. */
extern BOOL Sendn(int sock, void* buff, size_t bufflen);
#define SENDN(...) (_EXIT_ON_FALSE(Sendn, ##__VA_ARGS__))

/* Receives all requested bytes to socket stream. */
extern BOOL Recvn(int sock, void* buff, size_t bufflen);
#define RECVN(...) (_EXIT_ON_FALSE(Recvn, ##__VA_ARGS__))

/* Receives byte by byte until delimiter is encountered (or buffer is exceeded). Variable recvbytes can be ignored if set to NULL. */
extern BOOL Recvuntil(int sock, void* buff, size_t bufflen, unsigned char delimiter, size_t* recvbytes);
#define RECVUNTIL(...) (_EXIT_ON_FALSE(Recvuntil, ##__VA_ARGS__))

/* Gets socket option for SO_REUSEADDR. */
extern BOOL GetSockOpt_ReuseAddr(int sock, BOOL* enabled);
#define GETSOCKOPT_REUSEADDR(...) (_EXIT_ON_FALSE(GetSockOpt_ReuseAddr, ##__VA_ARGS__))

/* Sets socket option for SO_REUSEADDR. */
extern BOOL SetSockOpt_ReuseAddr(int sock, BOOL enable);
#define SETSOCKOPT_REUSEADDR(...) (_EXIT_ON_FALSE(SetSockOpt_ReuseAddr, ##__VA_ARGS__))

/* Gets socket option for SO_RCVTIMEO. */
extern BOOL GetSockOpt_RcvTimeo(int sock, long* sec, long* usec);
#define GETSOCKOPT_RCVTIMEO(...) (_EXIT_ON_FALSE(GetSockOpt_RcvTimeo, ##__VA_ARGS__))

/* Sets socket option for SO_RCVTIMEO. */
extern BOOL SetSockOpt_RcvTimeo(int sock, long sec, long usec);
#define SETSOCKOPT_RCVTIMEO(...) (_EXIT_ON_FALSE(SetSockOpt_RcvTimeo, ##__VA_ARGS__))

/* Gets socket option for SO_SNDTIMEO. */
extern BOOL GetSockOpt_SndTimeo(int sock, long* sec, long* usec);
#define GETSOCKOPT_SNDTIMEO(...) (_EXIT_ON_FALSE(GetSockOpt_SndTimeo, ##__VA_ARGS__))

/* Sets socket option for SO_SNDTIMEO. */
extern BOOL SetSockOpt_SndTimeo(int sock, long sec, long usec);
#define SETSOCKOPT_SNDTIMEO(...) (_EXIT_ON_FALSE(SetSockOpt_SndTimeo, ##__VA_ARGS__))

/* Gets socket option for IP_TTL (IPv4 sockets only). */
extern BOOL GetSockOpt_TTL(int sock, int* ttl);
#define GETSOCKOPT_TTL(...) (_EXIT_ON_FALSE(GetSockOpt_TTL, ##__VA_ARGS__))

/* Sets socket option for IP_TTL (IPv4 sockets only). */
extern BOOL SetSockOpt_TTL(int sock, int ttl);
#define SETSOCKOPT_TTL(...) (_EXIT_ON_FALSE(SetSockOpt_TTL, ##__VA_ARGS__))

#endif
