#include "tcpserver.h"

/* TCP listen port. */
const char* g_Port = "1234";

BOOL argfunc(int flag, const char* value) {
	
	/* Allow and parse only -p flag. */
	if (flag == 'p') {
		g_Port = value;
		return TRUE;
	}
	return FALSE;
	
}

void sendclimsg(int sock, unsigned char code, char* msg) {
	
	/* Print message. */
	fprintf(stderr, "%s\n", msg);
	
	/* Send code. */
	if (Sendn(sock, &code, 1) == FALSE) return;
	
	/* Send message. */
	Sendn(sock, msg, strlen(msg) + 1);
	
}

int main(int argc, char** argv) {
	
	/* Parse arguments. */
	if (Parseargs(argc, argv, "p:", argfunc, 0) == FALSE) {
		fprintf(stderr, "Usage: ./tcpserver [-p port]\n");
		return 1;
	}
	
	/* Resolve port service. */
	struct sockaddr_in addr;
	GETADDRINFO(NULL, g_Port, PROTOCOL_TCP, &addr);
	
	/* Create, bind and listen on TCP socket. */
	int sock;
	CREATEBINDSOCKET(TYPE_STREAM, PROTOCOL_TCP, &addr, &sock);
	SETSOCKOPT_REUSEADDR(sock, TRUE);
	LISTEN(sock, 1);
	
	/* Endless serve. */
	while (TRUE) {
	
		/* Accept client. */
		struct sockaddr_in cliaddr;
		int clisock;
		if (Accept(sock, &cliaddr, &clisock) == FALSE) continue;
		
		/* Print connect message. */
		char ip[NTOP_MAXLEN];
		unsigned short port = ntohs(cliaddr.sin_port);
		if (Ntop(&cliaddr.sin_addr, ip, sizeof(ip)) == FALSE) { close(clisock); continue; };
		printf("Connected client %s:%d\n", ip, port);
		
		/* Receive offset from client. */
		size_t offset;
		if (Recvn(clisock, &offset, sizeof(offset)) == FALSE) { close(clisock); continue; }
		offset = ntohl(offset);
		
		/* Receive file name. */
		size_t length;
		char filename[512];
		if (Recvuntil(clisock, filename, sizeof(filename), '\0', &length) == FALSE) { close(clisock); continue; }
		filename[length] = '\0';
		
		/* Print request message. */
		printf("Requested file '%s' from %u offset\n", filename, offset);
		
		/* Check for illegal character. */
		if (strchr(filename, '/') == NULL) {
			
			/* Check if file exists. */
			if (access(filename, F_OK) == 0) {
				
				/* Check for read access. */
				if (access(filename, R_OK) == 0) {
					
					/* Open file. */
					FILE* f = fopen(filename, "rb");
					if (f != NULL) {
						
						/* Seek to offset. */
						if (fseek(f, offset, SEEK_SET) == 0) {
							
							/* Send status. */
							unsigned char status = 0x00;
							Sendn(clisock, &status, 1);
							
							/* Send in chunks. */
							while (TRUE) {
								char buff[CHUNK_SIZE];
								size_t bufflen = fread(buff, 1, sizeof(buff), f);
								Sendn(clisock, buff, bufflen);
								if (feof(f)) break;
							}
							
							/* Print sent message. */
							printf("Sent file\n");
							
						} else sendclimsg(clisock, 0x03, "File cannot be seeked.");
					
						/* Close file. */
						fclose(f);
					
					} else sendclimsg(clisock, 0x03, "File cannot be opened.");
					
				} else sendclimsg(clisock, 0x02, "Permission denied");
				
			} else sendclimsg(clisock, 0x01, "File doesn't exist");
			
		} else sendclimsg(clisock, 0x01, "File name contains illegal '/' character");
		
		/* Close client socket. */
		close(clisock);
		
		/* Print disconnect message. */
		printf("Disconnected client\n");
	
	}
	
	return 0;
	
}
