#include "wrappers.h"

/* Non-concurrent getaddrinfo error number. */
int _gai_errno = 0;

/* Parses arguments. */
BOOL Parseargs(int argc, char* const argv[], const char* validflags, BOOL (*flagfunc)(int flag, const char* arg), int additionalparams) {
	int opt;
	while ((opt = getopt(argc, argv, validflags)) != -1)
		if (flagfunc(opt, optarg) == FALSE) return FALSE;
	if (argc - optind != additionalparams) return FALSE;
	for (int i = optind; i < argc; ++i)
		if (flagfunc(ADDITIONAL_PARAM + i - optind, argv[i]) == FALSE) return FALSE;
	return TRUE;
}

/* IPv4 address network to presentation. */
BOOL Ntop(struct in_addr* addr, char* buff, size_t bufflen) {
	return (inet_ntop(AF_INET, addr, buff, bufflen) == NULL) ? FALSE : TRUE;
}

/* IPv6 address network to presentation. */
BOOL Ntop6(struct in6_addr* addr, char* buff, size_t bufflen) {
	return (inet_ntop(AF_INET6, addr, buff, bufflen) == NULL) ? FALSE : TRUE;
}

/* Presentation to network IPv4 address. */
BOOL Pton(char* presentation, struct in_addr* addr) {
	return (inet_pton(AF_INET, presentation, addr) == 1) ? TRUE : FALSE;
}

/* Presentation to network IPv6 address. */
BOOL Pton6(char* presentation, struct in6_addr* addr) {
	return (inet_pton(AF_INET6, presentation, addr) == 1) ? TRUE : FALSE;
}

/* Almost native wrapper to getaddrinfo. */
static BOOL _Getaddrinfo(int family, int protocol, int flags, const char* hostname, const char* service, struct sockaddr* result) {
	struct addrinfo hints = { 0 };
	hints.ai_family = family;
	hints.ai_protocol = protocol;
	hints.ai_flags = flags;
	struct addrinfo* results = NULL;
	if ((_gai_errno = getaddrinfo(hostname, service, &hints, &results)) == 0) {
		assert(((family == AF_INET) ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6)) == results->ai_addrlen); /* make sure sizes will fit, just a precaussion */
		memcpy(result, results->ai_addr, results->ai_addrlen);
		freeaddrinfo(results);
		return TRUE;
	}
	return FALSE;
}

/* Getaddrinfo wrapper for IPv4. */
BOOL Getaddrinfo(const char* hostname, const char* service, PROTOCOL protocol, struct sockaddr_in* result) {
	return _Getaddrinfo(AF_INET, protocol, ((hostname == NULL) ? AI_PASSIVE : 0), hostname, service, (struct sockaddr*)result);
}

/* Getaddrinfo wrapper for IPv6. */
BOOL Getaddrinfo6(const char* hostname, const char* service, PROTOCOL protocol, struct sockaddr_in6* result) {
	return _Getaddrinfo(AF_INET6, protocol, ((hostname == NULL) ? AI_PASSIVE : 0), hostname, service, (struct sockaddr*)result);
}

/* Create IPv4 socket. */
BOOL Socket(TYPE type, PROTOCOL protocol, int* result) {
	int sock = socket(PF_INET, type, protocol);
	if (sock == -1) return FALSE;
	*result = sock;
	return TRUE;
}

/* Create IPv6 socket. */
BOOL Socket6(TYPE type, PROTOCOL protocol, int* result) {
	int sock = socket(PF_INET6, type, protocol);
	if (sock == -1) return FALSE;
	*result = sock;
	return TRUE;
}

/* Close socket. */
BOOL Close(int sock) {
	return (close(sock) == 0) ? TRUE : FALSE;
}

/* Bind IPv4 socket to address. */
BOOL Bind(int sock, struct sockaddr_in* addr) {
	return (bind(sock, (struct sockaddr*)addr, sizeof(struct sockaddr_in)) == 0) ? TRUE : FALSE;
}

/* Bind IPv6 socket to address. */
BOOL Bind6(int sock, struct sockaddr_in6* addr) {
	return (bind(sock, (struct sockaddr*)addr, sizeof(struct sockaddr_in6)) == 0) ? TRUE : FALSE;
}

/* Create and bind IPv4 socket. */
BOOL CreateBindSocket(TYPE type, PROTOCOL protocol, struct sockaddr_in* addr, int* result) {
	int sock;
	if (Socket(type, protocol, &sock) == FALSE) return FALSE;
	if (Bind(sock, addr) == FALSE) {
		close(sock);
		return FALSE;
	}
	*result = sock;
	return TRUE;
}

/* Create and bind IPv6 socket. */
BOOL CreateBindSocket6(TYPE type, PROTOCOL protocol, struct sockaddr_in6* addr, int* result) {
	int sock;
	if (Socket6(type, protocol, &sock) == FALSE) return FALSE;
	if (Bind6(sock, addr) == FALSE) {
		close(sock);
		return FALSE;
	}
	*result = sock;
	return TRUE;
}

/* Listen on socket. */
BOOL Listen(int sock, int maxconn) {
	return (listen(sock, maxconn) == 0) ? TRUE : FALSE;
}

/* Accept IPv4 connection and create socket with client. */
BOOL Accept(int sock, struct sockaddr_in* addr, int* result) {
	socklen_t len = sizeof(struct sockaddr_in);
	int clisock = accept(sock, (struct sockaddr*)addr, (addr == NULL) ? NULL : &len);
	if (clisock == -1) return FALSE;
	*result = clisock;
	return TRUE;
}

/* Accept IPv6 connection and create socket with client. */
BOOL Accept6(int sock, struct sockaddr_in6* addr, int* result) {
	socklen_t len = sizeof(struct sockaddr_in6);
	int clisock = accept(sock, (struct sockaddr*)addr, (addr == NULL) ? NULL : &len);
	if (clisock == -1) return FALSE;
	*result = clisock;
	return TRUE;
}

/* Connect to IPv4 server listener. */
BOOL Connect(int sock, struct sockaddr_in* addr) {
	return (connect(sock, (struct sockaddr*)addr, sizeof(struct sockaddr_in)) == 0) ? TRUE : FALSE;
}

/* Connect to IPv6 server listener. */
BOOL Connect6(int sock, struct sockaddr_in6* addr) {
	return (connect(sock, (struct sockaddr*)addr, sizeof(struct sockaddr_in6)) == 0) ? TRUE : FALSE;
}

/* Create socket and connect to IPv4 server listener. */
BOOL CreateConnectSocket(TYPE type, PROTOCOL protocol, struct sockaddr_in* addr, int* result) {
	int sock;
	if (Socket(type, protocol, &sock) == FALSE) return FALSE;
	if (Connect(sock, addr) == FALSE) {
		close(sock);
		return FALSE;
	}
	*result = sock;
	return TRUE;
}

/* Create socket and connect to IPv6 server listener. */
BOOL CreateConnectSocket6(TYPE type, PROTOCOL protocol, struct sockaddr_in6* addr, int* result) {
	int sock;
	if (Socket6(type, protocol, &sock) == FALSE) return FALSE;
	if (Connect6(sock, addr) == FALSE) {
		close(sock);
		return FALSE;
	}
	*result = sock;
	return TRUE;
}

/* Send a (datagram) packet to IPv4 address via socket. */
BOOL Sendto(int sock, void* buff, size_t bufflen, struct sockaddr_in* to, size_t* sentbytes) {
	ssize_t sent = sendto(sock, buff, bufflen, 0, (struct sockaddr*)to, sizeof(struct sockaddr_in));
	if (sent == -1) return FALSE;
	if (sentbytes != NULL) *sentbytes = sent;
	return TRUE;
}

/* Send a (datagram) packet to IPv6 address via socket. */
BOOL Sendto6(int sock, void* buff, size_t bufflen, struct sockaddr_in6* to, size_t* sentbytes) {
	ssize_t sent = sendto(sock, buff, bufflen, 0, (struct sockaddr*)to, sizeof(struct sockaddr_in6));
	if (sent == -1) return FALSE;
	if (sentbytes != NULL) *sentbytes = sent;
	return TRUE;
}

/* Receive a (datagram) packet from IPv4 address via socket. Variable recvbytes can be ignored if set to NULL. */
BOOL Recvfrom(int sock, void* buff, size_t bufflen, struct sockaddr_in* to, size_t* recvbytes) {
	socklen_t len = sizeof(struct sockaddr_in);
	ssize_t recv = recvfrom(sock, buff, bufflen, 0, (struct sockaddr*)to, &len);
	if (recv == -1) return FALSE;
	if (recvbytes != NULL) *recvbytes = recv;
	return TRUE;
}

/* Receive a (datagram) packet from IPv6 address via socket. Variable recvbytes can be ignored if set to NULL. */
BOOL Recvfrom6(int sock, void* buff, size_t bufflen, struct sockaddr_in6* to, size_t* recvbytes) {
	socklen_t len = sizeof(struct sockaddr_in6);
	ssize_t recv = recvfrom(sock, buff, bufflen, 0, (struct sockaddr*)to, &len);
	if (recv == -1) return FALSE;
	if (recvbytes != NULL) *recvbytes = recv;
	return TRUE;
}

/* Write data to socket stream */
BOOL Write(int sock, void* buff, size_t bufflen, size_t* sentbytes) {
	ssize_t sent = write(sock, buff, bufflen);
	if (sent == -1) return FALSE;
	if (sentbytes != NULL) *sentbytes = sent;
	return TRUE;
}

/* Read data from socket stream. */
BOOL Read(int sock, void* buff, size_t bufflen, size_t* recvbytes, BOOL* isEOF) {
	ssize_t recv = read(sock, buff, bufflen);
	if (recv == -1) return FALSE;
	if (recvbytes != NULL) *recvbytes = recv;
	if (isEOF != NULL) *isEOF = (recv == 0) ? TRUE : FALSE;
	return TRUE;
}

/* Send all requested bytes to socket stream. */
BOOL Sendn(int sock, void* buff, size_t bufflen) {
	return (send(sock, buff, bufflen, MSG_WAITALL) == -1) ? FALSE : TRUE;
}

/* Receive all requested bytes to socket stream. */
BOOL Recvn(int sock, void* buff, size_t bufflen) {
	return (recv(sock, buff, bufflen, MSG_WAITALL) == -1) ? FALSE : TRUE;
}

/* Receive byte by byte until delimiter is encountered (or buffer is exceeded). */
BOOL Recvuntil(int sock, void* buff, size_t bufflen, unsigned char delimiter, size_t* recvbytes) {
	size_t recv;
	for (recv = 0; recv < bufflen; ++recv) {
		if (Recvn(sock, (unsigned char*)buff + recv, 1) == FALSE) break;
		if (*((unsigned char*)buff + recv) == delimiter) { recv++; break; };
	}
	if (recvbytes != NULL) *recvbytes = recv;
	return TRUE;
}

/* Get socket option for SO_REUSEADDR. */
BOOL GetSockOpt_ReuseAddr(int sock, BOOL* enabled) {
	socklen_t len = sizeof(BOOL);
	return (getsockopt(sock, SOL_SOCKET, SO_REUSEADDR, enabled, &len) == 0) ? TRUE : FALSE;
}

/* Set socket option for SO_REUSEADDR. */
BOOL SetSockOpt_ReuseAddr(int sock, BOOL enable) {
	return (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(BOOL)) == 0) ? TRUE : FALSE;
}

/* Get socket option for SO_RCVTIMEO. */
BOOL GetSockOpt_RcvTimeo(int sock, long* sec, long* usec) {
	struct timeval tv = { 0 };
	socklen_t len = sizeof(tv);
	BOOL result = (getsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, &len) == 0) ? TRUE : FALSE;
	if (result == FALSE) return FALSE;
	*sec = tv.tv_sec;
	*usec = tv.tv_usec;
	return TRUE;
}

/* Set socket option for SO_RCVTIMEO. */
BOOL SetSockOpt_RcvTimeo(int sock, long sec, long usec) {
	struct timeval tv = { sec, usec };
	return (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == 0) ? TRUE : FALSE;
}

/* Get socket option for SO_SNDTIMEO. */
BOOL GetSockOpt_SndTimeo(int sock, long* sec, long* usec) {
	struct timeval tv = { 0 };
	socklen_t len = sizeof(tv);
	BOOL result = (getsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, &len) == 0) ? TRUE : FALSE;
	if (result == FALSE) return FALSE;
	*sec = tv.tv_sec;
	*usec = tv.tv_usec;
	return TRUE;
}

/* Set socket option for SO_SNDTIMEO. */
BOOL SetSockOpt_SndTimeo(int sock, long sec, long usec) {
	struct timeval tv = { sec, usec };
	return (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) == 0) ? TRUE : FALSE;
}

/* Get socket option for IP_TTL (IPv4 sockets only). */
BOOL GetSockOpt_TTL(int sock, int* ttl) {
	socklen_t len = sizeof(int);
	return (getsockopt(sock, IPPROTO_IP, IP_TTL, ttl, &len) == 0) ? TRUE : FALSE;
}

/* Set socket option for IP_TTL (IPv4 sockets only). */
BOOL SetSockOpt_TTL(int sock, int ttl) {
	return (setsockopt(sock, IPPROTO_IP, IP_TTL, &ttl, sizeof(int)) == 0) ? TRUE : FALSE;
}
